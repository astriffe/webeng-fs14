@extends('layouts.default')
@section('content')
<div class="sixteen wide column">
<h1 class="ui header">{{ $project->title }}</h1>
    <table class="ui sortable table segment">
        <thead>
            <tr>
                <th>Task</th>
                <th>Start</th>
                <th>End</th>
                <th>Description</th>
                <th>Time</th>
            </tr>
        </thead>
        <tbody>
        @foreach($data->records as $record)
            <tr>
                <td>{{ $record->task->title }}</td>
                <td>{{ $record->start }}</td>
                <td>{{ $record->end }}</td>
                <td>{{ $record->description }}</td>
                <td>{{ $record->present()->time(true) }}</td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th colspan="4"></th>
            <th>{{ $data->total }}</th>
        </tr>
        </tfoot>
    </table>
</div>
@stop