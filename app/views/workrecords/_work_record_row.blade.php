<tr class="<?= ($work_record->isRunning()) ? 'positive' : ''; ?>" data-work-record-id="{{ $work_record->id }}" data-work-record-tracking="<?= ($work_record->isRunning()) ? 'true' : 'false'; ?>" data-project-id="{{ $work_record->project_id }}" data-task-id="{{ $work_record->task_id }}">
    <td>
        @if ($work_record->project_id && $work_record->task_id)
            {{ $work_record->project->customer->title }} / {{ $work_record->project->title }} / {{ $work_record->task->title }}
        @elseif ($work_record->project_id && !$work_record->task_id)
            Quick-Start: {{ $work_record->project->customer->title }} / {{ $work_record->project->title }} / Please choose a task
        @elseif ($work_record->task_id && ! $work_record->project_id)
            Quick-Start: Please choose a Project / {{ $work_record->task->title }}
        @else
            Quick-Start: Please choose a project and task
        @endif
        <span class="description">{{ $work_record->description }}</span>
    </td>
    <td class="actions">
        @if ($work_record->isRunning())
            <div class="ui icon button mini teal stop-tracking tracking-button"><i class="icon time"></i> <span class="timer">{{ $work_record->present()->time(true) }}</span></div>
        @else
            <div class="ui icon button mini start-tracking tracking-button"><i class="icon time"></i> <span class="timer">{{ $work_record->present()->time() }}</span></div>
        @endif
        <div class="ui icon button mini edit-work-record"><i class="icon edit"></i></div>
        <div class="ui icon button red mini delete-work-record"><i class="icon trash"></i></div>
    </td>
</tr>