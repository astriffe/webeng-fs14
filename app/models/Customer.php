<?php

/**
 * Model Customer
 *
 */
class Customer extends Model {

    /**
     * @var array
     */
    public static $create_rules = array(
        'title' => 'required',
        'company_id' => 'required',
        'email' => 'required|email',
    );

    /**
     * @var array
     */
    public static $update_rules = array(
        'title' => 'required',
        'email' => 'required|email',
    );

    /**
     * @var array
     */
    protected $guarded = array('id');


    /**
     * Get Customers of provided instance
     *
     * @param $query
     * @param Instance|int $instance
     * @return mixed
     */
    public function scopeOfInstance($query, $instance)
    {
        $instance_id = ($instance instanceof Instance) ? $instance->id : $instance;

        return $query->whereInstanceId($instance_id);
    }


    /**
     * Get company
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function instance()
    {
        return $this->belongsTo('Instance');
    }


    /**
     * Get projects
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects()
    {
        return $this->hasMany('Project');
    }

}