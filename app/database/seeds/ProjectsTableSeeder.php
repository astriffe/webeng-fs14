<?php

class ProjectsTableSeeder extends Seeder {

	public function run()
	{

        DB::table('projects')->truncate();

        foreach(range(1, 10) as $i)
		{
			Project::create(array(
                'customer_id' => $i,
                'title' => "Project $i",
            ));
		}
	}

}