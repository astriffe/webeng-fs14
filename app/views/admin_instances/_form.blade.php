<div class="two fields">
    <div class="field @if($errors->has('title')) error @endif">
        {{ Form::label('title', 'Company', array('class' => 'ui label')) }}
        <div class="ui left labeled input">
            {{ Form::text('title') }}
            <div class="ui corner label">
                <i class="icon asterisk"></i>
            </div>
        </div>
    </div>
    <div class="field">
        {{ Form::label('street', 'Street', array('class' => 'ui label')) }}
        <div class="ui left labeled input">
            {{ Form::text('street') }}
        </div>
    </div>
</div>
<div class="two fields">
    <div class="field">
        {{ Form::label('zip', 'ZIP', array('class' => 'ui label')) }}
        <div class="ui left labeled input">
            {{ Form::text('zip') }}
        </div>
    </div>
    <div class="field">
        {{ Form::label('city', 'City', array('class' => 'ui label')) }}
        <div class="ui left labeled input">
            {{ Form::text('city') }}
        </div>
    </div>
</div>
<div class="two fields">
    <div class="field">
        {{ Form::label('tel', 'Phone', array('class' => 'ui label')) }}
        <div class="ui left labeled input">
            {{ Form::text('tel') }}
        </div>
    </div>
    <div class="field">
        {{ Form::label('fax', 'Fax', array('class' => 'ui label')) }}
        <div class="ui left labeled input">
            {{ Form::text('fax') }}
        </div>
    </div>
</div>
<div class="field @if($errors->has('email')) error @endif">
    {{ Form::label('email', 'E-Mail', array('class' => 'ui label')) }}
    <div class="ui left labeled input">
        {{ Form::email('email') }}
        <div class="ui corner label">
            <i class="icon asterisk"></i>
        </div>
    </div>
</div>

