<?php

class AdminRolesController extends \BaseController
{


    public function __construct()
    {
        parent::__construct();

        // Check for permission to administrate roles
        $this->beforeFilter(function () {
            if ( ! $this->user->hasPermission('administrate_roles')) {
                App::abort(403);
            }
        });
    }


    /**
     * Display a listing of roles
     *
     * @return Response
     */
    public function index()
    {
        $roles = Role::all();

        return View::make('admin_roles.index', compact('roles'));
    }


    /**
     * Show the form for creating a new role
     *
     * @return Response
     */
    public function create()
    {
        $permissions = Permission::all();

        return View::make('admin_roles.create', compact('permissions'));
    }


    /**
     * Store a newly created role in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            $role = Role::create(array_merge(Input::except('permissions')));
            $role->permissions()->sync(Input::get('permissions'));
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('admin.roles.index')->with('success', 'Successfully created Role ' . Input::get('name'));

    }


//    /**
//     * Display the specified user.
//     *
//     * @param  int $id
//     * @return Response
//     */
//    public function show($id)
//    {
//        $role = User::findOrFail($id);
//
//        return View::make('admin_roles.show', compact('user'));
//    }


    /**
     * Show the form for editing the specified role.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::all();

        return View::make('admin_roles.edit', compact('role', 'permissions'));
    }


    /**
     * Update the specified role in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $role = Role::findOrFail($id);
        try {
            $role->update(Input::except('permissions'));
            $role->permissions()->sync(Input::get('permissions'));
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('admin.roles.index')->with('success', 'Successfully updated Role ' . Input::get('name'));
    }


    /**
     * Remove the specified role from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return Redirect::route('admin_roles.index');
    }

}
