<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Model implements UserInterface, RemindableInterface
{

    use UserTrait, RemindableTrait;


    /**
     * @var array
     */
    public static $create_rules = array(
        'username' => 'required|unique:users',
        'instance_id' => 'required',
        'password' => 'required|min:3|confirmed'
    );


    /**
     * @var array
     */
    public static $update_rules = array(
        'username' => 'required',
        'password' => 'sometimes|required|min:3|confirmed'
    );

    /**
     * The users role. 0 stands for no role / not logged in. The higher the role, the more rights the user has.
     */
    /*    private $role = 0;*/

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    /**
     * @var array
     */
    protected $guarded = array('id', 'password_confirmation');

    /**
     * Holds the permissions for this user, cached
     *
     * @var array
     */
    protected $permissions;


    /**
     * When updating, adjust update rules: the username must be unique except for the id of the current user
     *
     * @param array $attributes
     * @return bool|int
     * @throws ValidationException
     */
    public function update(array $attributes = array())
    {
        static::$update_rules['username'] = 'required|unique:users,username,' . $this->id;
        static::validate($attributes, 'update');

        return parent::update($attributes);
    }


    /**
     * Get users of given instance
     *
     * @param $query
     * @param int|Instance $instance
     */
    public function scopeOfInstance($query, $instance)
    {
        $instance_id = ($instance instanceof Instance) ? $instance->id : $instance;

        return $query->where('instance_id', '=', $instance_id);
    }


    /**
     * Get Roles
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('Role', 'role_users');
    }


    /**
     * Get Instance
     *
     * @return Instance
     */
    public function instance()
    {
        return $this->belongsTo('Instance');
    }


    /**
     * @param array $role_ids
     * @param bool $allow_system_roles
     */
    public function setRoles(array $role_ids=array(), $allow_system_roles = false)
    {
        $ids = $role_ids;
        if ( ! $allow_system_roles) {
            foreach ($role_ids as $role_id) {
                $role = Role::find($role_id);
                if ( ! $role->system) $ids[] = $role->id;
            }
        }

        $this->roles()->sync($ids);
    }


    /**
     * @param string $role_name Name of a role
     * @return bool
     */
    public function hasRole($role_name)
    {
        $role = $this->roles->filter(function ($role) use ($role_name) {
            return $role->name == $role_name;
        })->first();

        return ($role) ? true : false;
    }


    /**
     * Checks if the user has the given permission by a role
     *
     * @param string $permission_name Name of a permission
     * @return bool
     */
    public function hasPermission($permission_name)
    {
        $this->loadPermissions();

        return isset($this->permissions[$permission_name]);
    }


    /**
     * Setter for the password attribute
     *
     * @param string $password
     */
    public function setPasswordAttribute($password)
    {
        // Don't set empty passwords
        if ( ! $password) return;

        $this->attributes['password'] = Hash::make($password);
    }

    /**
     * Load all permissions from assigned roles
     */
    protected function loadPermissions()
    {
        if ( ! is_null($this->permissions)) {
            return;
        }

        $this->permissions = array();
        /** @var Role $role */
        foreach ($this->roles as $role) {
            $this->permissions = array_merge($this->permissions, $role->permissions->keyBy('name')->toArray());
        }
    }
}
