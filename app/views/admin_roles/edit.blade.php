@extends('layouts.default')
@section('content')
<div class="sixteen wide column">

    <h1 class="ui header">Edit Role</h1>

    <div class="ui form segment">
        {{ Form::model($role, array('method' => 'PUT', 'route' => array('admin.roles.update', $role->id))) }}
        @include('admin_roles._form')
        <button class="ui teal submit primary button">Update</button>
        {{ Form::close() }}
    </div>
</div>
@stop