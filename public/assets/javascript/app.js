
var app = {

    init : function() {

        this.initSemanticUI();
        this.initTimer();
        this.initDatePicker();

    },

    initTimer : function() {

        setInterval(function() {
            var $tr = $('#tracking-table tr[data-work-record-tracking="true"]');
            if ( ! $tr.length) return;
            var work_record_id = $tr.attr('data-work-record-id');
            $.get(config.urls.get_accurate_time + work_record_id + '?formatted=true', function(time) {
                $tr.find('span.timer').html(time);
            });
        }, 61000);

    },

    initSemanticUI : function() {

        $('.ui.dropdown').dropdown();

        $('.message .close').on('click', function() {
            $(this).closest('.message').fadeOut();
        });

    },

    initDatePicker : function() {

        $('.pick-date').datetimepicker({
            format : 'Y-m-d',
            timepicker : false,
            onChangeDateTime : function(dp, $input) {
                window.location = config.urls.tracking + '/' + $input.val();
            }
        });

    },

    stopTracking : function(work_record_id) {

        var $tr = $('#tracking-table tr[data-work-record-id="' + work_record_id  + '"]');
        $tr.removeClass('positive')
            .attr('data-work-record-tracking', 'false');

        var $button = $tr.find('.tracking-button');
        $button.removeClass('teal')
            .removeClass('stop-tracking')
            .addClass('start-tracking');

        $.post(config.urls.stop_tracking + work_record_id, function(response) {
            if (response.type == 'error') {
                alert(response.message);
            }
        });
    },

    startTracking : function(work_record_id) {

        // Check if there is another workRecord running atm, if so, stop it first
        var $tr = $('#tracking-table tr[data-work-record-tracking="true"]');
        if ($tr.length) {
            var _work_record_id = $tr.attr('data-work-record-id');
            this.stopTracking(_work_record_id);
        }

        // Track the current
        $tr = $('#tracking-table tr[data-work-record-id="' + work_record_id  + '"]');
        $tr.addClass('positive')
            .attr('data-work-record-tracking', 'true');

        var $button = $tr.find('.tracking-button');
        $button.addClass('teal')
            .addClass('stop-tracking')
            .removeClass('start-tracking');

        $.post(config.urls.start_tracking + work_record_id, function(response) {
            if (response.type == 'error') {
                alert(response.message);
            }
        });

    },

    openEditModal : function(work_record_id) {

        var $tr = $('#tracking-table tr[data-work-record-id="' + work_record_id  + '"]');
        var project_id = $tr.attr('data-project-id');
        var task_id = $tr.attr('data-task-id');
        var description = $tr.find('span.description').text();
        var $form = $('#tracking-form')
            .clone()
            .attr('id', 'work-record-edit-form');
        $form.find('button').remove();
        $form.find('input[name="project_id"]').val(project_id);
        $form.find('input[name="task_id"]').val(task_id);
        $form.find('textarea[name="description"]').html(description);
        $form.append("<input type='hidden' name='id' value='" + work_record_id +"'>");
        $('#modal-edit-work-record').find('.content').html('').append($form);
        $('#modal-edit-work-record').modal('show');
        $('.ui.dropdown').dropdown();

    },

    deleteWorkRecord : function(work_record_id) {

        $.post(config.urls.delete_work_record + work_record_id, function(response) {
            if (response.type == 'error') {
                alert(response.message);
            } else {
                var $tr = $('#tracking-table tr[data-work-record-id="' + work_record_id  + '"]');
                $tr.fadeOut();
            }
        });
    }
}

$(document).ready(function(){

    app.init();

    $(document).on('click', '.start-tracking', function() {
        var work_record_id = $(this).closest('tr').attr('data-work-record-id');
        app.startTracking(work_record_id);
    });

    $(document).on('click', '.stop-tracking', function() {
        var work_record_id = $(this).closest('tr').attr('data-work-record-id');
        app.stopTracking(work_record_id);
    });

    $(document).on('click', '.edit-work-record', function() {
        var work_record_id = $(this).closest('tr').attr('data-work-record-id');
        app.openEditModal(work_record_id);
    });

    $(document).on('click', '.delete-work-record', function() {
        if (confirm('Delete?')) {
            var work_record_id = $(this).closest('tr').attr('data-work-record-id');
            app.deleteWorkRecord(work_record_id);
        }
    });

    $(document).on('click', '#update_work_record', function() {
        $('#work-record-edit-form').submit();
    })


    $(document).on('click', '.pick-date', function() {
        $(this).datetimepicker('show');
    });

    $('#tracking-form').on('submit', function(e) {
        var $form = $(this);

        // Send Form via AJAX
        e.preventDefault();
        var url = $form.attr('action');
        var data = $form.serialize();
        $.post(url, data, function(data) {
            // Add task to the tracking table
            var $table = $('#tracking-table');
            $table.find('tbody').append(data);
            // Start the newly added task
            var work_record_id = $(data).filter('tr').attr('data-work-record-id');
            app.startTracking(work_record_id);
        })
    });

    $(document).on('submit', '#work-record-edit-form', function(e) {
        var $form = $(this);
        var work_record_id = $form.find('input[name="id"]').val();
        e.preventDefault();
        var data = $form.serialize();
        $.post(config.urls.update_work_record + work_record_id, data, function(response) {
            // Replace the tr in the tracking table
            var $tr = $('#tracking-table').find('tr[data-work-record-id="' + work_record_id + '"]');
            $tr.replaceWith(response);
        });
    });

});