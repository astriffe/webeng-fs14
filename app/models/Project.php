<?php

/**
 * Project Model
 *
 */
class Project extends Model
{


    /**
     * @var array
     */
    public static $rules = array(
        'title' => 'required',
        'customer_id' => 'required',
    );


    /**
     * @var array
     */
    protected $guarded = array('id');


    /**
     * Get projects of given instance
     *
     * @param $query
     * @param int|Instance $instance
     */
    public function scopeOfInstance($query, $instance)
    {
        $instance_id = ($instance instanceof Instance) ? $instance->id : $instance;

        return $query->join('customers', 'projects.customer_id', '=', 'customers.id')
            ->where('customers.instance_id', '=', $instance_id)->select('projects.*');
    }


    /**
     * Get Customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customer()
    {
        return $this->belongsTo('Customer');
    }


    /**
     * Get work records
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workRecords()
    {
        return $this->hasMany('WorkRecord');
    }

}