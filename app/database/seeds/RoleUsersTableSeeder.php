<?php

class RoleUsersTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('role_users')->truncate();

        // First ten users have normal user roles
//        $this->insert(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10), array(5));

        // Create superadmin, admin etc. user roles
//        $this->insert(array(11), array(1));
//        $this->insert(array(12), array(2));
//        $this->insert(array(13), array(3));
//        $this->insert(array(14), array(4));
//        $this->insert(array(15), array(5));
        $this->insert(array('1'), array('1'));
        $this->insert(array('2'), array('2'));
    }

    protected function insert($users, $roles) {
        foreach ($users as $user) {
            foreach ($roles as $role) {
                DB::insert("INSERT INTO role_users (user_id, role_id) values (?, ?)", array($user, $role));
            }
        }
    }

}