<?php

/**
 * Model Task
 *
 */
class Task extends Model {

    /**
     * @var array
     */
    protected $guarded = array('id');

    /**
     * @var array
     */
    public static $create_rules = array(
        'instance_id' => 'required',
        'title' => 'required',
        'cost' => 'required|numeric'
    );

    /**
     * @var array
     */
    public static $update_rules = array(
        'title' => 'required',
        'cost' => 'required|numeric'
    );


    /**
     * Get Tasks of provided instance
     *
     * @param $query
     * @param Instance|int $instance
     * @return mixed
     */
    public function scopeOfInstance($query, $instance)
    {
        $instance_id = ($instance instanceof Instance) ? $instance->id : $instance;

        return $query->whereInstanceId($instance_id);
    }


    /**
     * Get Company
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('Company');
    }

}