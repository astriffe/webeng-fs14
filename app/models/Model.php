<?php

/**
 * Class Model
 *
 * Base class for all models, moves validation to the model (thinner controllers) ;)
 */
class Model extends Eloquent
{

    /**
     * Put validation rules in this array for subclasses, if create and update rules are the same
     *
     * @var array
     */
    public static $rules = array();

    /**
     * Rules only applied on creation of object
     *
     * @var array
     */
    public static $create_rules = array();

    /**
     * Rules only applied on update of object
     *
     * @var array
     */
    public static $update_rules = array();

    /**
     * Validate before creating
     *
     * @param array $attributes
     * @return static
     * @throws ValidationException
     */
    public static function create(array $attributes)
    {
        static::validate($attributes, 'create');

        return parent::create($attributes);
    }


    /**
     * Validate before updating
     *
     * @param array $attributes
     * @return bool|int
     * @throws ValidationException
     */
    public function update(array $attributes = array())
    {
        static::validate($attributes, 'update');

        return parent::update($attributes);
    }


    /**
     * Validate data against rules
     *
     * @param array $data
     * @param string $mode create|update
     * @throws ValidationException
     */
    protected static function validate(array $data, $mode)
    {
        $rules = static::$rules;
        if ($mode == 'create' && count(static::$create_rules)) $rules = static::$create_rules;
        if ($mode == 'update' && count(static::$update_rules)) $rules = static::$update_rules;

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $class = get_called_class();
            throw new ValidationException("Could not save model {$class}", $validator->errors()->toArray());
        }
    }


} 