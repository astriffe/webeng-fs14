<!DOCTYPE html>
<html>
<head>
	@include('includes.head')
</head>

<body>
<div id="container" class="ui page grid">

    <div class="sixteen wide column">
        <!-- Main Menu -->
        <div class="ui inverted menu">

            <div class="item">CHRONOS</div>

            <div class="right menu">

                <!-- Track time -->
                @if($current_user->hasPermission('track_time'))
                <a class="item @if(Route::currentRouteName() == 'track.index') active @endif" href="<?= route('track.index') ?>">
                    <i class="time icon"></i> Track Time
                </a>
                @endif

                <!-- Administrate -->
                <!-- Only Admin or above can access. -->
                @if($current_user->hasRole('superadmin') || $current_user->hasRole('admin'))
                <div class="ui dropdown item">
                      Administrate <i class="dropdown icon"></i>
                      <div class="menu">

                        @if($current_user->hasPermission('administrate_instances'))
                        <a class="item" href="<?= route('admin.instances.index') ?>">Instances</a>
                        @endif

                        @if($current_user->hasPermission('administrate_users'))
                        <a class="item" href="<?= route('admin.users.index') ?>">Users</a>
                        @endif

                        @if($current_user->hasPermission('administrate_roles'))
                        <a class="item" href="<?= route('admin.roles.index') ?>">Roles</a>
                        @endif
                    </div>
                </div>
                @endif


                <!-- Manage -->
                <!-- Only Project Managers or above can access. -->
                @if($current_user->hasRole('companyowner') || $current_user->hasRole('projectmgr'))
                <div class="ui dropdown item">
                      <i class="settings icon"></i>
                      Manage <i class="dropdown icon"></i>
                      <div class="menu">

                        @if($current_user->hasPermission('manage_projects'))
                        <a class="item" href="<?= route('projects.index') ?>">Projects</a>
                        @endif

                        @if($current_user->hasPermission('manage_tasks'))
                        <a class="item" href="<?= route('tasks.index') ?>">Tasks</a>
                        @endif

                        @if($current_user->hasPermission('manage_clients'))
                        <a class="item" href="<?= route('customers.index') ?>">Clients</a>
                        @endif

                        @if($current_user->hasPermission('manage_users'))
                        <a class="item" href="<?= route('users.index') ?>">Users</a>
                        @endif

                    </div>
                </div>
                @endif

                <!-- Report -->
                @if($current_user->hasPermission('report_user'))
                    <a class="item @if(Route::currentRouteName() == 'report.index') active @endif" href="{{ route('report.index') }}"><i class="print icon"></i>Report</a>
                @endif

                <!-- User -->
                @if($current_user->id > 0)
                <div class="ui dropdown item">
                    <i class="user icon"></i>{{ $current_user->username }} <i class="dropdown icon"></i>
                    <div class="menu">
                        <a class="item" href="{{ route('profile.edit', array($current_user->id)) }}">Profile</a>
                        <a class="item" href="<?= action('HomeController@doLogout') ?>"><strong>Logout</strong></a>
                    </div>
                </div>
                @endif
            </div>
        </div>

        <!-- Flash messages across app -->
        @if(Session::has('success'))
            <div class="ui success message">
              <i class="close icon"></i>
                {{ Session::get('success') }}
            </div>
        @endif

        @if($errors->has())
            <div class="ui error message">
              <i class="close icon"></i>
                <div class="header">Errors</div>
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        @endif


        <div id="content" class="ui grid" role="main">
            @yield('content')
        </div>

        <div id="footer" class="row">
            @include('includes.footer')
        </div>
    </div>
</div>

<script type="text/javascript">
    var config = JSON.parse('{{ $js_config }}');
</script>


<script type="text/javascript" src="{{ asset('assets/javascript/jquery-1.11.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/javascript/semantic.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/javascript/jquery.datetimepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/javascript/app.js') }}"></script>


</body>
</html>
