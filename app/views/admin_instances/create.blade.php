@extends('layouts.default')
@section('content')
<div class="sixteen wide column">

    <h1 class="ui header">Create Instance</h1>

    <div class="ui form segment">
        {{ Form::open(array('route' => array('admin.instances.store'))) }}
        @include('admin_instances._form')
        <button class="ui teal submit primary button">Create</button>
        {{ Form::close() }}
    </div>
</div>
@stop