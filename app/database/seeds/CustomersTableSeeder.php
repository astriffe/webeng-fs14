<?php

class CustomersTableSeeder extends Seeder {

	public function run()
	{

        DB::table('customers')->truncate();

        foreach(range(1, 10) as $i)
		{
			Customer::create(array(
                'company_id' => $i,
                'title' => "Customer$i",
                'email' => "customer$i@example.com"
            ));
		}
	}

}