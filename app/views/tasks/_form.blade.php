<div class="field @if($errors->has('title')) error @endif">
    {{ Form::label('title', 'Title', array('class' => 'ui label')) }}
    <div class="ui left labeled input">
        {{ Form::text('title') }}
        <div class="ui corner label">
            <i class="icon asterisk"></i>
        </div>
    </div>
</div>
<div class="field">
    {{ Form::label('description', 'Description', array('class' => 'ui label')) }}
    <div class="ui left labeled input">
        {{ Form::textarea('description') }}
    </div>
</div>
<div class="field @if($errors->has('cost')) error @endif">
    {{ Form::label('cost', 'Cost', array('class' => 'ui label')) }}
    <div class="ui left labeled input">
        {{ Form::text('cost') }}
        <div class="ui corner label">
            <i class="icon asterisk"></i>
        </div>
    </div>
</div>