<?php

/**
 * Class Instance
 */
class Instance extends Model
{

    /**
     * @var array
     */
    public static $rules = array(
        'title' => 'required',
        'email' => 'required|email',
    );


    /**
     * @var array
     */
    protected $guarded = array('id');


    /**
     * Return all users belonging to this company
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('User');
    }


    /**
     * Return all customers
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customers()
    {
        return $this->hasMany('Customer');
    }


    /**
     * Return all tasks
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks()
    {
        return $this->hasMany('Task');
    }


    /**
     * Return all projects of all customers
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function projects()
    {
        return $this->hasManyThrough('Project', 'Customer');
    }

}