<?php

class UsersTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('users')->truncate();

        // Passwords are hashed in the setter of password attribute
        // Let's remove the confirmed password validation rule for seeding...
        User::$create_rules['password'] = '';

        // Create superadmin
        User::create(array(
            'username' => 'superadmin',
            'password' => 'superadmin',
            'instance_id' => 0,
        ));

        // Create admin
        User::create(array(
            'username' => 'admin',
            'password' => 'admin',
            'instance_id' => 0,
        ));

//        $co = User::create(array(
//            'username' => 'companyowner',
//            'password' => 'companyowner',
//            'company_id' => 1,
//        ));
//
//        $pmgr = User::create(array(
//            'username' => 'projectmgr',
//            'password' => 'projectmgr',
//            'company_id' => 1,
//        ));
//
//        $user = User::create(array(
//            'username' => 'user',
//            'password' => 'user',
//            'company_id' => 1,
//        ));

    }
}
