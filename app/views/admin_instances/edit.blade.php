@extends('layouts.default')
@section('content')
<div class="sixteen wide column">

    <h1 class="ui header">Edit Instance</h1>

    <div class="ui form segment">
        {{ Form::model($instance, array('method' => 'PUT', 'route' => array('admin.instances.update', $instance->id))) }}
        @include('admin_instances._form')
        <button class="ui teal submit primary button">Update</button>
        {{ Form::close() }}
    </div>

</div>
@stop