@extends('layouts.default')
@section('content')
<div class="sixteen wide column">
<h1 class="ui header">Users</h1>
    <table class="ui sortable table segment">
        <thead>
            <tr>
                <th class="ascending">Username</th>
                <th>Role(s)</th>
                <th class="actions">Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->username }}</td>
                <td>{{ $user->roles->implode('title', ', ') }}</td>
                <td class="actions">
                    <a href="{{ route('users.edit', array($user->id)) }}" class="ui mini icon button"><i class="icon edit"></i></a>
                    {{--<a href="{{ route('users.destroy', array($user->id)) }}" class="ui mini button icon red"><i class="icon trash"></i></a>--}}
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th colspan="4">
                <a class="ui teal labeled icon mini button" href="{{ route('users.create') }}">
                    <i class="plus icon"></i>
                    Add User
                </a>
            </th>
        </tr>
        </tfoot>
    </table>
</div>
@stop