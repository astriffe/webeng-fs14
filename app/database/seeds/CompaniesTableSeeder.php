<?php

class CompaniesTableSeeder extends Seeder {

	public function run()
	{

        DB::table('companies')->truncate();

        foreach(range(1, 10) as $index)
		{
			Instance::create(array(
                'title' => "Company{$index}",
                'email' => "company{$index}@example.com"
            ));
		}
	}

}