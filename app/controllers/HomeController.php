<?php

class HomeController extends BaseController
{


    public function index(){
        return $this->redirectToSectionBasedOnPermission();
    }

    public function showWelcome()
    {
        return View::make('hello');
    }


    public function showLogin()
    {
        return View::make('login');
    }


    public function doLogin()
    {
        // validate the info, create rules for the inputs
        $rules = array(
            'username' => 'required|alphaNum', // make sure the username is alphanumeric
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('login')
                ->withErrors($validator)// send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {

            // create our user data for the authentication
            $userdata = array(
                'username' => Input::get('username'),
                'password' => Input::get('password')
            );

            if (Auth::attempt($userdata)) {

                // Redirect based on permissions
                return $this->redirectToSectionBasedOnPermission();

            } else {

                return Redirect::to('login')->withErrors('Wrong Credentials')->withInput(Input::except('password'));

            }

        }

    }


    public function doLogout()
    {
        Auth::logout();

        return Redirect::to('login')->withSuccess('You are logged out. See you soon!');
    }


    /**
     * Perform redirect (internally) to the correct section
     *
     * @return mixed
     */
    protected function redirectToSectionBasedOnPermission()
    {
        if (Auth::user()->hasPermission('track_time')) {
            return Redirect::route('track.index')->withSuccess('Welcome!');
        } elseif (Auth::user()->hasPermission('administrate_instances')) {
            return Redirect::route('admin.instances.index')->withSuccess('Welcome, admin!');
        }
    }

}
