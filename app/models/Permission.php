<?php

class Permission extends Model {


    /**
     * @var array
     */
    public static $rules = array(
        'title' => 'required',
        'name' => 'required'
    );


    /**
     * @var array
     */
    protected $guarded = array('id');


    /**
     * Return all roles of this permission
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('Role');
    }

}