<?php

class WorkRecordTest extends TestCase {

    /**
     * @var WorkRecord
     */
    protected $work_record;

    public function setUp()
    {
        parent::setUp();
        /** @var WorkRecord $work_record */
        $work_record = WorkRecord::find(1);
        // Delete all existing WorkSessions for a clean start
        $work_record->workSessions()->delete();
        $this->work_record = $work_record;
    }

    /**
     * Calling start must create a new work session
     *
     */
    public function testStart()
    {
        $this->work_record->start();
        $this->assertTrue(count($this->work_record->workSessions()) == 1);
    }

//    /**
//     * @depends testStart
//     */
//    public function testStop()
//    {
//        $this->work_record->stop();
//        // The work session should have a stopped timestamp
//        $work_session = WorkSession::where('work_record_id', '=', $this->work_record->id)->get();
//        var_dump($work_session);
//        $this->assertFalse($work_session->end == '0000-00-00 00:00:00');
//    }


}
