@extends('layouts.error')
@section('content')
    <div class="sixteen wide column">
    <div class="ui error message">
        <i class="close icon"></i>
        <div class="header">
            Page not found
        </div>
        Sorry, this page does not seem to exist, maybe go <a href="{{ route('home') }}">back to the homepage</a>?
    </div>
    </div>
@stop