<div class="field @if($errors->has('title')) error @endif">
    {{ Form::label('title', 'Title', array('class' => 'ui label')) }}
    <div class="ui left labeled input">
        {{ Form::text('title') }}
        <div class="ui corner label">
            <i class="icon asterisk"></i>
        </div>
    </div>
</div>
<div class="field">
    {{ Form::label('description', 'Description', array('class' => 'ui label')) }}
    <div class="ui left labeled input">
        {{ Form::textarea('description') }}
    </div>
</div>
<div class="field @if($errors->has('customer_id')) error @endif">
    {{ Form::label('customer_id', 'Client', array('class' => 'ui label')) }}
    <div class="ui left labeled input">
        <div class="ui selection dropdown">
          <input type="hidden" name="customer_id" value="{{ isset($project) ? $project->customer_id : '' }}">
          <div class="default text">Select Client</div>
          <i class="dropdown icon"></i>
          <div class="menu">
            @foreach ($customers as $customer)
                <div class="item" data-value="{{ $customer->id }}">{{ $customer->title }}</div>
            @endforeach
          </div>
            <div class="ui corner label">
                <i class="icon asterisk"></i>
            </div>
        </div>
    </div>
</div>