<?php

class ProfileController extends \BaseController
{


    public function __construct()
    {
        parent::__construct();

        // Check for permission to edit profile, user id must match the id of current user
        $this->beforeFilter(function () {
            if (Request::segment(2) != $this->user->id) {
                App::abort(403);
            }
        });
    }


    /**
     * Show the form for editing the specified user.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return View::make('profile.edit', compact('user'));
    }


    /**
     * Update the specified user in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        /** @var User $user */
        $user = User::findOrFail($id);
        try {
            $data = Input::all();
            if ( ! $data['password']) {
                unset ($data['password']);
                unset($data['password_confirmation']);
            }
            $user->update($data);
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('profile.edit', array($id))->with('success', 'Successfully updated profile');
    }

}
