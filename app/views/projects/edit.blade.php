@extends('layouts.default')
@section('content')
<div class="sixteen wide column">

    <h1 class="ui header">Edit Project</h1>

    <div class="ui form segment">
        {{ Form::model($project, array('method' => 'PUT', 'route' => array('projects.update', $project->id))) }}
        @include('projects._form')
        <button class="ui teal submit primary button">Update</button>
        {{ Form::close() }}
    </div>
</div>
@stop