<?php

/**
 * Class CustomersController
 *
 */
class CustomersController extends \BaseController
{


    public function __construct()
    {
        parent::__construct();

        // Check for permission to manage tasks
        $this->beforeFilter(function() {
            if ( ! $this->user->hasPermission('manage_clients')) {
                App::abort(403);
            }
        });
    }


    /**
     * Display a listing of customers
     *
     * @return Response
     */
    public function index()
    {
        $customers = Customer::ofInstance($this->user->instance)->orderBy('title')->get();

        return View::make('customers.index', compact('customers'));
    }


    /**
     * Show the form for creating a new customer
     *
     * @return Response
     */
    public function create()
    {
        return View::make('customers.create');
    }


    /**
     * Store a newly created customer in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            Customer::create(array_merge(Input::all(), array('company_id' => $this->user->company_id)));
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('customers.index')->with('success', 'Successfully created Client ' . Input::get('title'));
    }


    /**
     * Display the specified customer.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);

        $this->checkPermissionAndReturn($customer);

        return View::make('customers.show', compact('customer'));
    }


    /**
     * Show the form for editing the specified customer.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);

        $this->checkPermissionAndReturn($customer);

        return View::make('customers.edit', compact('customer'));
    }


    /**
     * Update the specified customer in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $customer = Customer::findOrFail($id);

        $this->checkPermissionAndReturn($customer);

        try {
            $customer->update(Input::all());
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('customers.index')->with('success', 'Successfully updated Client ' . Input::get('title'));
    }


    /**
     * Remove the specified customer from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Customer::destroy($id);

        return Redirect::route('customers.index');
    }


    /**
     * Only the logged in user can modify his own project objects
     *
     * @param Customer $customer
     */
    protected function checkPermissionAndReturn(Customer $customer)
    {
        if ($customer->instance_id != $this->user->instance_id) {
            App::abort(403);
        }
    }


}
