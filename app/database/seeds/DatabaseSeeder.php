<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('UsersTableSeeder');
//        $this->call('CompaniesTableSeeder');
//        $this->call('CustomersTableSeeder');
//        $this->call('ProjectsTableSeeder');
//        $this->call('TasksTableSeeder');
//        $this->call('WorkRecordsTableSeeder');
//        $this->call('WorkSessionsTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('RoleUsersTableSeeder');
        $this->call('PermissionsTableSeeder');
	}

}
