<?php

class AdminUsersController extends \BaseController
{


    public function __construct()
    {
        parent::__construct();

        // Check for permission to administrate users
        $this->beforeFilter(function () {
            if ( ! $this->user->hasPermission('administrate_users')) {
                App::abort(403);
            }
        });
    }


    /**
     * Display a listing of users
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();

        return View::make('admin_users.index', compact('users'));
    }


    /**
     * Show the form for creating a new user
     *
     * @return Response
     */
    public function create()
    {
        $instances = Instance::all();
        $roles = $this->user->hasRole('superadmin') ? Role::all() : Role::instance()->get();

        return View::make('admin_users.create', compact('instances', 'roles'));
    }


    /**
     * Store a newly created user in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            $data = Input::except('roles');
            if ( ! $data['password']) unset($data['password']);
            $user = User::create($data);
            $allow_system_roles = $this->user->hasRole('superadmin') ? true : false;
            $user->setRoles(Input::get('roles'), $allow_system_roles);
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('admin.users.index')->with('success', 'Successfully created User ' . Input::get('username'));

    }


    /**
     * Display the specified user.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return View::make('admin_users.show', compact('user'));
    }


    /**
     * Show the form for editing the specified user.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $instances = Instance::all();
        $roles = $this->user->hasRole('superadmin') ? Role::all() : Role::instance()->get();

        return View::make('admin_users.edit', compact('user', 'instances', 'roles'));
    }


    /**
     * Update the specified user in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $user = User::findOrFail($id);
        try {
            $data = Input::except('roles');
            if ( ! $data['password']) unset($data['password']);
            $user->update($data);
            $allow_system_roles = $this->user->hasRole('superadmin') ? true : false;
            $user->setRoles(Input::get('roles'), $allow_system_roles);
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('admin.users.index')->with('success', 'Successfully updated User ' . Input::get('username'));

    }


    /**
     * Remove the specified user from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return Redirect::route('admin_users.index');
    }

}
