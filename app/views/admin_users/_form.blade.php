<div class="one field">
    <div class="field @if($errors->has('username')) error @endif">
        {{ Form::label('username', 'Username', array('class' => 'ui label')) }}
        <div class="ui left labeled input">
            {{ Form::text('username') }}
            <div class="ui corner label">
                <i class="icon asterisk"></i>
            </div>
        </div>
    </div>

    <div class="field @if($errors->has('password')) error @endif">
        {{ Form::label('password', 'Password', array('class' => 'ui label')) }}
        <div class="ui left labeled password input">
            {{ Form::password('password') }}
            <div class="ui corner label">
                <i class="icon lock"></i>
            </div>
        </div>
    </div>

    <div class="field @if($errors->has('password_confirmation')) error @endif">
        {{ Form::label('password_confirmation', 'Password confirmation', array('class' => 'ui label')) }}
        <div class="ui left labeled password input">
            {{ Form::password('password_confirmation') }}
            <div class="ui corner label">
                <i class="icon lock"></i>
            </div>
        </div>
    </div>

    <!-- Company -->
    <div class="field @if($errors->has('instance_id')) error @endif">
        {{ Form::label('instance_id', 'Instance', array('class' => 'ui label')) }}
        <div class="ui left labeled input">
            <div class="ui selection dropdown">
              <input type="hidden" name="instance_id" value="{{ $user->instance_id or Input::old('instance_id') }}">
              <div class="default text">Select Company</div>
              <i class="dropdown icon"></i>
              <div class="menu">
                @foreach ($instances as $instance)
                    <div class="item" data-value="{{ $instance->id }}">{{ $instance->title }}</div>
                @endforeach
              </div>
                <div class="ui corner label">
                    <i class="icon asterisk"></i>
                </div>
            </div>
        </div>
    </div>

    @if (isset($roles))
        <div class="field">
            {{ Form::label('roles', 'Roles', array('class' => 'ui label')) }}
            @foreach($roles as $role)
                <div class="field">
                    <div class="ui checkbox">
                      <input id="role_{{ $role->id }}" type="checkbox" name="roles[]" value="{{ $role->id }}"@if((isset($user) && $user->roles->contains($role)) || Input::get("roles.{$role->id}")) checked @endif>
                      <label for="role_{{ $role->id }}">{{ $role->title }}</label>
                    </div>
                </div>
            @endforeach
        </div>
    @endif

</div>