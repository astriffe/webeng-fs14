<!DOCTYPE html>
<html>
<head>
	@include('includes.head')
</head>

<body>
<div id="container" class="ui page grid">

    <div class="sixteen wide column">
        <!-- Main Menu -->
        <div class="ui inverted menu">

            <div class="item">CHRONOS</div>
        </div>

        <div id="content" class="ui grid" role="main">
            @yield('content')
        </div>

        <div id="footer" class="row">
            @include('includes.footer')
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ asset('assets/javascript/jquery-1.11.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/javascript/semantic.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/javascript/jquery.datetimepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/javascript/app.js') }}"></script>


</body>
</html>
