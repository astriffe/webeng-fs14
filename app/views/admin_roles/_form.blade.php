<div class="one field">
    <div class="field @if($errors->has('title')) error @endif">
        {{ Form::label('title', 'Title', array('class' => 'ui label')) }}
        <div class="ui left labeled input">
            {{ Form::text('title') }}
            <div class="ui corner label">
                <i class="icon asterisk"></i>
            </div>
        </div>
    </div>

    <div class="field @if($errors->has('name')) error @endif">
        {{ Form::label('name', 'Name (only small chars and underline)', array('class' => 'ui label')) }}
        <div class="ui left labeled input">
            {{ Form::text('name') }}
            <div class="ui corner label">
                <i class="icon asterisk"></i>
            </div>
        </div>
    </div>

    <div class="field @if($errors->has('description')) error @endif">
        {{ Form::label('description', 'Description', array('class' => 'ui label')) }}
        <div class="ui left labeled input">
            {{ Form::textarea('description') }}
        </div>
    </div>

    <div class="field">
        {{ Form::label('permissions', 'Permissions', array('class' => 'ui label')) }}
        @foreach($permissions as $permission)
            <div class="field">
                <div class="ui checkbox">
                    <input id="permission_{{ $permission->id }}" type="checkbox" name="permissions[]" value="{{ $permission->id }}"@if((isset($role) && $role->permissions->contains($permission)) || Input::get("roles.{$permission->id}")) checked @endif>
                    <label for="permission_{{ $permission->id }}">{{ $permission->title }}</label>
                </div>
            </div>
        @endforeach
    </div>

</div>