@extends('layouts.default')
@section('content')
<div class="sixteen wide column">

    <h1 class="ui header">Edit Client</h1>

    <div class="ui form segment">
        {{ Form::model($customer, array('method' => 'PUT', 'route' => array('customers.update', $customer->id))) }}
        @include('customers._form')
        <button class="ui teal submit primary button">Update</button>
        {{ Form::close() }}
    </div>

</div>
@stop