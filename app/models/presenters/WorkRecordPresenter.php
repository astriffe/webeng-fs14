<?php


class WorkRecordPresenter
{

    /**
     * @var WorkRecord
     */
    protected $work_record;


    public function __construct(WorkRecord $work_record)
    {
        $this->work_record = $work_record;
    }


    /**
     * Return formatted time duration from seconds to H:MM
     *
     * @param bool $accurate
     * @return string
     */
    public function time($accurate = false)
    {
        return ($accurate) ? $this->format($this->work_record->getAccurateTime()) : $this->format($this->work_record->time);
    }


    /**
     * Format given seconds to H:MM
     *
     * @param int $seconds
     * @return string
     */
    public static function format($seconds)
    {
        $minutes = $seconds / 60;
        $hours = floor($minutes / 60);
        $min = str_pad($minutes % 60, 2, '0', STR_PAD_LEFT);
        return "$hours:$min";
    }

} 