<?php

/**
 * Created by PhpStorm.
 * User: alexander
 * Date: 01.12.14
 * Time: 17:54
 */
class ReportController extends BaseController
{


    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Display a listing of projects
     *
     * @return Response
     */
    public function index()
    {
        $projects = Project::ofInstance($this->user->instance)->get();

        return View::make('report.index', compact('projects'));

//        $projects = Project::ofInstance($this->user->instance)->get();
//        $selectID = Input::get('Select_id');
//        //$selectedProject= Project::find($selectID);
//        // TODO remove bulk project loading
//        $selectedProject = $projects[0];
//
//        $records = array();
//        $times = array();
//        if ($selectedProject != null) {
//            // TODO add project data, which tasks spent the most time?
//            foreach ($selectedProject->workRecords() as &$rec) {
//                // TODO assign workRecord to tasks array
//                array_push($records[$rec->project()->title], $rec->getAccurateTime());
//            }
//
//            // Sum up times
//            foreach ($records as &$task) {
//                $sum = 0;
//                foreach ($task as &$e) {
//                    $sum += $e;
//                }
//                $times[$task] = $sum;
//
//            }
//
//            return View::make('report.index', compact('projects', 'selectedProject', 'times'));
//        }
//
//
//        return View::make('report.index', compact('projects'));
    }

    /**
     * Display report of given project
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $report = new ReportUser($this->user);
        $data = $report->getDataByProject($id);
        $project = Project::findOrFail($id);

        return View::make('report.show', compact('data', 'project'));
    }

} 