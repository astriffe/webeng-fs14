@extends('layouts.default')
@section('content')
<div class="sixteen wide column">
<h1 class="ui header">Projects</h1>
    <table class="ui sortable table segment">
        <thead>
            <tr>
                <th class="ascending">Title</th>
                <th>Description</th>
                <th>Client</th>
                <th class="actions">Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($projects as $project)
            <tr>
                <td>{{ $project->title }}</td>
                <td>{{ $project->description }}</td>
                <td>{{ $project->customer->title }}</td>
                <td class="actions">
                    <a href="{{ route('projects.edit', array($project->id)) }}" class="ui mini icon button"><i class="icon edit"></i></a>
                    {{--<a href="#" class="ui mini button icon red"><i class="icon trash"></i></a>--}}
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th colspan="4">
                <a class="ui teal labeled icon mini button" href="{{ route('projects.create') }}">
                    <i class="plus icon"></i>
                    Add Project
                </a>
            </th>
        </tr>
        </tfoot>
    </table>
</div>
@stop