<?php

class UsersController extends \BaseController
{


    public function __construct()
    {
        parent::__construct();

        // Check for permission to manage users
        $this->beforeFilter(function () {
            if ( ! $this->user->hasPermission('manage_users')) {
                App::abort(403);
            }
        });
    }


    /**
     * Display a listing of users
     *
     * @return Response
     */
    public function index()
    {
        $users = User::ofInstance($this->user->instance)->orderBy('username')->get();

        return View::make('users.index', compact('users'));
    }


    /**
     * Show the form for creating a new user
     *
     * @return Response
     */
    public function create()
    {
        $roles = Role::instance()->get();

        return View::make('users.create', compact('roles'));
    }


    /**
     * Store a newly created user in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            $user = User::create(array_merge(Input::except('roles'), array('instance_id' => $this->user->instance_id)));
            $user->setRoles(Input::get('roles'));
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('users.index')->with('success', 'Successfully created User ' . Input::get('username'));
    }


    /**
     * Display the specified user.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        $this->checkPermissionAndReturn($user);

        return View::make('users.show', compact('user'));
    }


    /**
     * Show the form for editing the specified user.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $this->checkPermissionAndReturn($user);

        $roles = Role::instance()->get();

        return View::make('users.edit', compact('user', 'roles'));
    }


    /**
     * Update the specified user in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $user = User::findOrFail($id);

        $this->checkPermissionAndReturn($user);

        try {
            $data = Input::except('roles');
            if ( ! $data['password']) unset($data['password']);
            $user->update($data);
            $user->setRoles(Input::get('roles'));
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('users.index')->with('success', 'Successfully updated User ' . Input::get('username'));
    }


    /**
     * Remove the specified user from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        return Redirect::route('users.index');
    }


    /**
     * Only the logged in user can modify his own project objects
     *
     * @param User $user
     */
    protected function checkPermissionAndReturn(User $user)
    {
        if ($user->instance_id != $this->user->instance_id) {
            App::abort(403);
        }
    }


}
