<?php

class TasksController extends \BaseController
{

    public function __construct()
    {
        parent::__construct();

        // Check for permission to manage tasks
        $this->beforeFilter(function() {
            if ( ! $this->user->hasPermission('manage_tasks')) {
                App::abort(403);
            }
        });
    }


    /**
     * Display a listing of tasks
     *
     * @return Response
     */
    public function index()
    {
        $tasks = Task::ofInstance($this->user->instance)->orderBy('title')->get();

        return View::make('tasks.index', compact('tasks'));
    }


    /**
     * Show the form for creating a new task
     *
     * @return Response
     */
    public function create()
    {
        return View::make('tasks.create');
    }


    /**
     * Store a newly created task in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            Task::create(array_merge(Input::all(), array('instance_id' => $this->user->instance_id)));
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('tasks.index')->with('success_message', 'Successfully created Task ' . Input::get('title'));
    }


    /**
     * Display the specified task.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $task = Task::findOrFail($id);

        $this->checkPermissionAndReturn($task);

        return View::make('tasks.show', compact('task'));
    }


    /**
     * Show the form for editing the specified task.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);

        $this->checkPermissionAndReturn($task);

        return View::make('tasks.edit', compact('task'));
    }


    /**
     * Update the specified task in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $task = Task::findOrFail($id);

        $this->checkPermissionAndReturn($task);

        try {
            $task->update(Input::all());
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('tasks.index')->with('success', 'Successfully updated Task ' . Input::get('title'));
    }


    /**
     * Remove the specified task from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Task::destroy($id);

        $this->checkPermissionAndReturn($task);

        return Redirect::route('tasks.index')->with('success', 'Deleted Task');
    }


    /**
     * Only the logged in user can modify his own project objects
     *
     * @param Task $task
     */
    protected function checkPermissionAndReturn(Task $task)
    {
        if ($task->instance_id != $this->user->instance_id) {
            App::abort(403);
        }
    }


}
