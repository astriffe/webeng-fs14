@extends('layouts.default')
@section('content')
<div class="sixteen wide column">

    <h1 class="ui header">Edit User Data</h1>

    <div class="ui form segment">
        {{ Form::model($user, array('method' => 'PUT', 'route' => array('users.update', $user->id))) }}
        @include('users._form')
        <button class="ui teal submit primary button">Update</button>
        {{ Form::close() }}
    </div>
</div>
@stop