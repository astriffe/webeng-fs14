<meta charset="utf-8">
<meta name="author" content="Striffeler / Wanzenried">

{{--<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,600,300' rel='stylesheet' type='text/css'>--}}
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Open+Sans:300italic,400,300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="{{ asset('assets/css/semantic.min.css') }}">
{{--<link rel="stylesheet" href="{{ asset('assets/semantic-ui/dist/semantic.min.css') }}">--}}
<link rel="stylesheet" href="{{ asset('assets/css/jquery.datetimepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">

<title>CHRONOS :: Time Tracking Tool</title>
