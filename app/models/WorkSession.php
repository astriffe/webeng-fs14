<?php

/**
 * Class WorkSession
 *
 * @author Stefan Wanzenried <sw@studer-raimman.ch>
 */

class WorkSession extends \Eloquent {

    /**
     * @var array
     */
    public static $rules = array(
        'work_record_id' => 'required',

    );

    /**
     * @var array
     */
    protected $guarded = array('id');


    /**
     * Get work record
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function workRecord()
    {
        return $this->belongsTo('workRecord');
    }


}