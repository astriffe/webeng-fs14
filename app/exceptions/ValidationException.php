<?php

/**
 * Class ValidationException
 *
 * Thrown if a model fails to save due to validation errors
 *
 */
class ValidationException extends Exception
{

    /**
     * @var array
     */
    protected $errors = array();


    /**
     * @param string $message
     * @param array $errors
     * @param int $code
     * @param null $previous
     */
    public function __construct($message, array $errors, $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->errors = $errors;
    }


    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

} 