@extends('layouts.default')
@section('content')

<div class="ui modal" id="modal-edit-work-record">
    <i class="close icon"></i>
    <div class="header">
        Edit Tracking Record
    </div>
    <div class="content ui fluid form segment"></div>
    <div class="actions">
        <div class="ui primary teal button" id="update_work_record">
            Save
        </div>
        <div class="ui button">
            Cancel
        </div>
    </div>
</div>

<div class="sixteen wide column">
    <div class="two column stackable ui grid">
        <div class="column">
            <h1 class="ui header"><?= strftime('%A, %e. %B %G', strtotime($date)) ?></h1>
        </div>
        <!-- Day picker -->
        <div class="column">
            @include('workrecords._calendar')
        </div>
    </div>
</div>

<div class="sixteen wide column">
    <div class="ui fluid form segment">
        <h3 class="ui header">Add new task to track</h3>
        <form action="{{ route('track.store') }}" method="POST" id="tracking-form">
        <div class="two fields">
            <div class="field">
                <label class="ui label">Project</label>
                <div class="ui fluid selection dropdown">
                    <input type="hidden" name="project_id">
                    <div class="default text">--Choose--</div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                        <?php foreach($projects as $project): ?>
                            <div class="item" data-value="<?= $project->id ?>"><?= $project->customer->title . ' / ' . $project->title ?></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="field">
                <label class="ui label">Task</label>
                <div class="ui fluid selection dropdown">
                    <input type="hidden" name="task_id">
                    <div class="default text">--Choose--</div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                        <?php foreach($tasks as $task): ?>
                            <div class="item" data-value="<?= $task->id ?>"><?= $task->title ?></div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="field">
            <label class="ui label">Remarks</label>
            <textarea class="ui textarea" name="description"></textarea>
        </div>
        <button id="track" class="ui teal submit primary button">
            Track
        </button>
        </form>
    </div>
    <div class="ui fluid form segment">
        <h3 class="ui header">Tracked Time</h3>
        <table class="ui basic table segment" id="tracking-table">
            <tbody>
            @foreach($work_records as $work_record)
                @include('workrecords._work_record_row', array('work_record' => $work_record))
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@stop