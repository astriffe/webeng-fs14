<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

# Homepage
Route::get('/', array('uses' => 'HomeController@index', 'as' => 'home'));

#	User authentication
Route::get('login', array('uses'=>'HomeController@showLogin'));
Route::post('login', array('uses' => 'HomeController@doLogin'));
Route::get('logout', array('uses' => 'HomeController@doLogout'));

// Resources (RESTFUL)
Route::resource('customers', 'CustomersController');
Route::resource('projects', 'ProjectsController');
Route::resource('tasks', 'TasksController');
Route::resource('users', 'UsersController');
Route::resource('report', 'ReportController', array('only' => array('index', 'show')));

// Profile
Route::resource('profile', 'ProfileController', array('only' => array('edit', 'update')));

// Administrating routes, for admins only
Route::resource('admin/users', 'AdminUsersController');
Route::resource('admin/instances', 'AdminInstancesController');
Route::resource('admin/roles', 'AdminRolesController');
//Route::resource('admin/permissions', 'AdminPermissionsController');

//Route::resource('work_records', 'WorkRecordsController');
// Let's create an ALIAS route "track" which displays the main GUI for tracking time
// The CRUD of WorkRecord objects is mostly done over AJAX
Route::get('track/{date?}', array('as' => 'track.index', 'uses' => 'WorkRecordsController@index'));
Route::post('track/store', array('as' => 'track.store', 'uses' => 'WorkRecordsController@store'));
Route::post('track/update/{id}', array('as' => 'track.update', 'uses' => 'WorkRecordsController@update'));
Route::post('track/start/{id}', array('as' => 'track.start', 'uses' => 'WorkRecordsController@start'));
Route::post('track/stop/{id}', array('as' => 'track.stop', 'uses' => 'WorkRecordsController@stop'));
Route::get('track/get_accurate_time/{id}', array('as' => 'track.accurate_time', 'uses' => 'WorkRecordsController@getAccurateTime'));
Route::post('track/delete/{id}', array('as' => 'track.delete', 'uses' => 'WorkRecordsController@delete'));


// Report Controller
//Route::get('report/{project_id?}', array('as' => 'report.show_project', 'uses' => 'ReportController@show_project'));
