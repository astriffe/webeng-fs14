<?php

class WorkSessionsController extends \BaseController {

	/**
	 * Display a listing of worksessions
	 *
	 * @return Response
	 */
	public function index()
	{
		$worksessions = Worksession::all();

		return View::make('worksessions.index', compact('worksessions'));
	}

	/**
	 * Show the form for creating a new worksession
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('worksessions.create');
	}

	/**
	 * Store a newly created worksession in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Worksession::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Worksession::create($data);

		return Redirect::route('worksessions.index');
	}

	/**
	 * Display the specified worksession.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$worksession = Worksession::findOrFail($id);

		return View::make('worksessions.show', compact('worksession'));
	}

	/**
	 * Show the form for editing the specified worksession.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$worksession = Worksession::find($id);

		return View::make('worksessions.edit', compact('worksession'));
	}

	/**
	 * Update the specified worksession in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$worksession = Worksession::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Worksession::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$worksession->update($data);

		return Redirect::route('worksessions.index');
	}

	/**
	 * Remove the specified worksession from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Worksession::destroy($id);

		return Redirect::route('worksessions.index');
	}

}
