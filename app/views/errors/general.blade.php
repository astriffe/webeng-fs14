@extends('layouts.error')
@section('content')
    <div class="sixteen wide column">
    <div class="ui error message">
        <i class="close icon"></i>
        <div class="header">
            Error
        </div>
        Sorry, there was an error. We try to fix it as soon as possible! In the meanwhile, maybe go <a href="{{ route('home') }}">back to the homepage</a>?
    </div>
    </div>
@stop