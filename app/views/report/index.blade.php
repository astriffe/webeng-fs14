@extends('layouts.default')
@section('content')
<div class="sixteen wide column">
<h1 class="ui header">Projects</h1>
    <table class="ui sortable table segment">
        <thead>
            <tr>
                <th class="ascending">Title</th>
                <th>Description</th>
                <th>Client</th>
                <th class="actions">Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($projects as $project)
            <tr>
                <td>{{ $project->title }}</td>
                <td>{{ $project->description }}</td>
                <td>{{ $project->customer->title }}</td>
                <td class="actions">
                    <a href="{{ route('report.show', array($project->id)) }}" class="ui icon mini button"><i class="icon checkmark"></i> Generate</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@stop