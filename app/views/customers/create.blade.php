@extends('layouts.default')
@section('content')
<div class="sixteen wide column">

    <h1 class="ui header">Create Client</h1>

    <div class="ui form segment">
        {{ Form::open(array('route' => array('customers.store'))) }}
        @include('customers._form')
        <button class="ui teal submit primary button">Create</button>
        {{ Form::close() }}
    </div>
</div>
@stop