{{--No model used for this simple script ;)--}}
<div class="ui small six item menu">
    @for($i=-4; $i<=0; $i++)
        <?php $_date = strtotime("{$i} days"); ?>
        <a href="{{ route('track.index', array('date' => date('Y-m-d', $_date))) }}" class="item<?= (date('Y-m-d', $_date) == $date) ? ' active' : '' ?>" style="text-align: center">
        <strong>{{ date('D', $_date) }}</strong>
        <br>
        {{ date('d.m', $_date) }}
        </a>
    @endfor
    <div class="item">
        <div class="ui mini icon button pick-date"><i class="icon calendar"></i> Pick</div>
    </div>
</div>
