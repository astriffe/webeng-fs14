@extends('layouts.default')
@section('content')
<div class="five wide center column">
    <h1 class="ui header">Login</h1>
	{{ Form::open(array('url' => 'login')) }}
        <div class="ui form segment">
        <div class="field">
            <label class="ui label">Username</label>
            <div class="ui left labeled icon input">
              <input type="text" name="username" placeholder="Username" value="{{{ Input::old('username') }}}">
              <i class="user icon"></i>
              <div class="ui corner label">
                <i class="icon asterisk"></i>
              </div>
            </div>
          </div>
          <div class="field">
            <label class="ui label">Password</label>
            <div class="ui left labeled icon input">
              <input type="password" name="password">
              <i class="lock icon"></i>
              <div class="ui corner label">
                <i class="icon asterisk"></i>
              </div>
            </div>
          </div>
          <button class="ui teal submit button">Login</button>
        </div>
	{{ Form::close() }}
@stop