<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/


/* Redirect users which are not logged in to Login page. */
App::before(function($request)
{
    if(Auth::guest() && $request->path() != 'login')
    {
        return Redirect::guest('login');
    }
});



App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});



/*
|--------------------------------------------------------------------------
| Authentication Filters for the different User Roles
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application and has the required user role.
|
*/
//Route::filter('isSuperadmin', function($route, $request)
//{
//    if(!Auth::check()) return Redirect::guest('login');
//    if( !in_array('superadmin', Auth::user()->roles->toArray()) ) {
//        return Redirect::to('/'); // Redirect home page
//    }
//});
//
//Route::filter('isAdmin', function($route, $request)
//{
//    if(!Auth::check()) return Redirect::guest('login');
//    if( !in_array('admin', Auth::user()->roles->toArray()) ) {
//        return Redirect::to('/'); // Redirect home page
//    }
//});
//
//Route::filter('isCompanyOwner', function($route, $request)
//{
//    if(!Auth::check()) return Redirect::guest('login');
//    if( !in_array('companyowner', Auth::user()->roles->toArray()) ) {
//        return Redirect::to('/'); // Redirect home page
//    }
//});
//
//Route::filter('isProjectManager', function($route, $request)
//{
//    if(!Auth::check()) return Redirect::guest('login');
//    if( !in_array('projectmgr', Auth::user()->roles->toArray()) ) {
//        return Redirect::to('/'); // Redirect home page
//    }
//});
//
//Route::filter('isUser', function($route, $request)
//{
//    if(!Auth::check()) return Redirect::guest('login');
//    if( !in_array('user', Auth::user()->roles->toArray()) ) {
//        return Redirect::to('/'); // Redirect home page
//    }
//});


/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
