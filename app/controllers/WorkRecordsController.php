<?php

class WorkRecordsController extends \BaseController
{

    const JSON_RESPONSE_ERROR = 'error';
    const JSON_RESPONSE_SUCCESS = 'success';


    public function __construct()
    {
        parent::__construct();

        // Check for permission to track time
        $this->beforeFilter(function () {
            if ( ! $this->user->hasPermission('track_time')) {
                if (Request::ajax()) {
                    return $this->jsonResponse(self::JSON_RESPONSE_ERROR, array('message' => 'Permission denied'));
                } else {
                    App::abort(403);
                }
            }
        });
    }


    /**
     * Display a listing of workrecords
     * By default the records of today are displayed, by passing a date string, records of the given day are displayed
     *
     * @param string $date Date format in format Y-m-d
     * @return Response
     */
    public function index($date = '')
    {
        $date = ($date) ? date('Y-m-d', strtotime($date)) : date('Y-m-d');
        $projects = Project::ofInstance($this->user->instance)->get();
        $tasks = Task::ofInstance($this->user->instance)->get();
        $work_records = WorkRecord::ofUser($this->user, $date);

        return View::make('workrecords.index',
            compact('projects', 'tasks', 'work_records'),
            array('date' => $date));
    }


    /**
     * Start tracking a work record (AJAX only)
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function start($id)
    {
        /** @var WorkRecord $work_record */
        $work_record = WorkRecord::findOrFail($id);
        $this->checkPermissionAndReturn($work_record);
        $work_record->startTracking();

        return $this->jsonResponse(self::JSON_RESPONSE_SUCCESS, array('message' => 'Started tracking a new record'));
    }


    /**
     * Start tracking a work record (AJAX only)
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function stop($id)
    {
        /** @var WorkRecord $work_record */
        $work_record = WorkRecord::findOrFail($id);
        $this->checkPermissionAndReturn($work_record);
        $work_record->stopTracking();

        return $this->jsonResponse(self::JSON_RESPONSE_SUCCESS, array('message' => 'Stopped tracking record'));
    }


    /**
     * Store a newly created WorkRecord object in storage (AJAX only)
     *
     * @return Response
     */
    public function store()
    {
        $data = array_merge(Input::all(), array('user_id' => $this->user->id));
        try {
            $work_record = WorkRecord::create($data);
        } catch (ValidationException $e) {
            return $this->jsonResponse(self::JSON_RESPONSE_ERROR, array('message' => implode(', ', $e->getErrors())));
        }

        return View::make('workrecords._work_record_row', compact('work_record'));
    }


    /**
     * Update the specified workrecord in storage (AJAX only)
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $work_record = WorkRecord::findOrFail($id);
        $this->checkPermissionAndReturn($work_record);

        try {
            $work_record->update(Input::all());
        } catch (ValidationException $e) {
            return $this->jsonResponse(self::JSON_RESPONSE_ERROR, array('message' => implode(', ', $e->getErrors())));
        }

        return View::make('workrecords._work_record_row', compact('work_record'));
    }


    /**
     * Delete a WorkRecord
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        /** @var WorkRecord $work_record */
        $work_record = WorkRecord::findOrFail($id);
        $this->checkPermissionAndReturn($work_record);

        try {
            $work_record->delete();
            return $this->jsonResponse(self::JSON_RESPONSE_SUCCESS, array('message' => 'Successfully delete WorkRecord'));
        } catch (Exception $e) {
            return $this->jsonResponse(self::JSON_RESPONSE_ERROR, array('message' => $e->getMessage()));
        }
    }

    /**
     * Get the accurate time of the given workrecord
     *
     * @param int $id
     * @return mixed
     */
    public function getAccurateTime($id)
    {
        /** @var WorkRecord $work_record */
        $work_record = WorkRecord::findOrFail($id);
        $formatted = (Input::get('formatted') == 'true') ? true : false;

        return ($formatted) ? $work_record->present()->time(true) : $work_record->time;
    }


    /**
     * Return a JSON response
     *
     * @param string $type error|success
     * @param array $params
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonResponse($type, array $params=array())
    {
        return Response::json(array('type' => $type, 'params' => $params));
    }


    /**
     * Only the logged in user can modify his own WorkRecord objects
     *
     * @param WorkRecord $work_record
     * @return \Illuminate\Http\JsonResponse
     */
    protected function checkPermissionAndReturn(WorkRecord $work_record)
    {
        if ( ! $work_record->user_id == $this->user->user_id) {
            return $this->jsonResponse(self::JSON_RESPONSE_ERROR, array('message' => 'Permission denied'));
        }
    }

}
