<?php

/**
 * Class ProjectsController
 *
 */
class ProjectsController extends \BaseController
{


    public function __construct()
    {
        parent::__construct();

        // Check for permission to manage projects
        $this->beforeFilter(function() {
            if ( ! $this->user->hasPermission('manage_projects')) {
                App::abort(403);
            }
        });

    }

    /**
     * Display a listing of projects
     *
     * @return Response
     */
    public function index()
    {
        $projects = Project::ofInstance($this->user->instance)->orderBy('title')->get();

        return View::make('projects.index', compact('projects'));
    }


    /**
     * Show the form for creating a new project
     *
     * @return Response
     */
    public function create()
    {
        $customers = Customer::ofInstance($this->user->instance)->orderBy('title')->get();

        return View::make('projects.create', compact('customers'));
    }


    /**
     * Store a newly created project in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            Project::create(Input::all());
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('projects.index')->with('success', "Successfully created project " . Input::get('title'));
    }


    /**
     * Display the specified project.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $project = Project::findOrFail($id);

        return View::make('projects.show', compact('project'));
    }


    /**
     * Show the form for editing the specified project.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $project = Project::findOrFail($id);

        $this->checkPermissionAndReturn($project);

        $customers = Customer::ofInstance($this->user->instance)->get();

        return View::make('projects.edit', compact('project', 'customers'));
    }


    /**
     * Update the specified project in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $project = Project::findOrFail($id);

        $this->checkPermissionAndReturn($project);

        try {
            $project->update(Input::all());
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('projects.index')->with('success', 'Successfully updated project ' . Input::get('title'));
    }


    /**
     * Remove the specified project from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Project::destroy($id);

        return Redirect::route('projects.index')->with('success', 'Successfully deleted project');
    }

    /**
     * Only the logged in user can modify his own project objects
     *
     * @param Project $project
     */
    protected function checkPermissionAndReturn(Project $project)
    {
        $customer = Customer::findOrFail($project->customer_id);
        if ($customer->instance_id != $this->user->instance_id) {
            App::abort(403);
        }
    }

}
