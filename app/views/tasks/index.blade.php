@extends('layouts.default')
@section('content')
<div class="sixteen wide column">
<h1 class="ui header">Tasks</h1>
    <table class="ui sortable table segment">
        <thead>
            <tr>
                <th class="ascending">Title</th>
                <th>Description</th>
                <th>Cost</th>
                <th class="actions">Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($tasks as $task)
            <tr>
                <td>{{ $task->title }}</td>
                <td>{{ $task->description }}</td>
                <td>{{ $task->cost }}</td>
                <td class="actions">
                    <a href="{{ route('tasks.edit', array($task->id)) }}" class="ui mini icon button"><i class="icon edit"></i></a>
                    {{--<a href="#" class="ui mini button icon red"><i class="icon trash"></i></a>--}}
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th colspan="4">
                <a class="ui teal labeled icon mini button" href="{{ route('tasks.create') }}">
                    <i class="plus icon"></i>
                    Add Task
                </a>
            </th>
        </tr>
        </tfoot>
    </table>
</div>
@stop