<?php

class PermissionsTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('permissions')->truncate();
        DB::table('permission_role')->truncate();

        Permission::create([
            'title' => 'Administrate Instances',
            'name' => 'administrate_instances',
            'description' => 'Administrate Instances (Admin Level)'
        ]);

        Permission::create([
            'title' => 'Administrate Users',
            'name' => 'administrate_users',
            'description' => 'Administrate Users (Admin Level)'
        ]);

        Permission::create([
            'title' => 'Administrate Roles',
            'name' => 'administrate_roles',
            'description' => 'Administrate Roles (Admin Level)',
        ]);

        Permission::create([
            'title' => 'Administrate Permissions',
            'name' => 'administrate_permissions',
            'description' => 'Administrate Permissions (Admin Level)',
        ]);

        Permission::create([
            'name' => 'manage_projects',
            'title' => 'Manage Projects',
            'description' => ''
        ]);

        Permission::create([
            'title' => 'Manage Tasks',
            'name' => 'manage_tasks',
            'description' => ''
        ]);

        Permission::create([
            'title' => 'Manage Clients',
            'name' => 'manage_clients',
            'description' => ''
        ]);

        Permission::create([
            'title' => 'Manage users',
            'name' => 'manage_users',
            'description' => 'Manage Users on Company Level',
        ]);

        Permission::create([
            'title' => 'Track Time',
            'name' => 'track_time',
            'description' => '',
        ]);

        Permission::create([
            'title' => 'Report of own tracking data',
            'name' => 'report_user',
            'description' => 'A user is able to generate reports of his own tracking data',
        ]);

        // Also create the pivot table

        // Super-Administrators have all the administrate permissions
        $this->insert(array(1), array(1, 2, 3, 4));

        // Administrators can administrate Companies and Users of the app
        $this->insert(array(2), array(1, 2));

        // Company Owner/Administrator can manage everything (company level)
        $this->insert(array(3), array(5, 6, 7, 8, 9, 10));

        // Project Manager can only manage projects and tasks (company level)
        $this->insert(array(4), array(5, 6, 9, 10));

        // User: Base Role allowing to track time and basic reporting (company level)
        $this->insert(array(5), array(9, 10));
    }

    protected function insert($roles, $permissions) {
        foreach ($roles as $role) {
            foreach ($permissions as $permission) {
                DB::insert("INSERT INTO permission_role (role_id, permission_id) values (?, ?)", array($role, $permission));
            }
        }
    }
}
