<?php

class RolesTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('roles')->truncate();

        Role::create([
            'name' => 'superadmin',
            'title' => 'Super Admin',
            'description' => 'Grants access to everything, including: Roles, instances and users of the app',
            'system' => true,
        ]);

        Role::create([
            'name' => 'admin',
            'title' => 'Administrator',
            'description' => 'Administrate instances and users of the app',
            'system' => true,
        ]);

        Role::create([
            'title' => 'Company Owner',
            'name' => 'companyowner',
            'description' => 'Administrator on instance level, including: Managing users, clients, projects and tasks'
        ]);

        Role::create([
            'title' => 'Project Manager',
            'name' => 'projectmgr',
            'description' => 'Manage projects and tasks'
        ]);

        Role::create([
            'title' => 'User',
            'name' => 'user',
            'description' => 'Basic permissions: Time tracking and report of own tracking data',
        ]);
    }
}
