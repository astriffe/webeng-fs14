@extends('layouts.default')
@section('content')
<div class="sixteen wide column">
<h1 class="ui header">Roles</h1>
    <table class="ui sortable table segment">
        <thead>
            <tr>
                <th class="ascending">Title</th>
                <th>Name</th>
                <th>Description</th>
                <th class="actions">Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($roles as $role)
            <tr>
                <td>{{ $role->title }}</td>
                <td>{{ $role->name }}</td>
                <td>{{ $role->description }}</td>
                <td class="actions">
                    <a href="{{ route('admin.roles.edit', array($role->id)) }}" class="ui mini icon button"><i class="icon edit"></i></a>
                    {{--<a href="{{ route('admin.roles.destroy', array($role->id)) }}" class="ui mini button icon red"><i class="icon trash"></i></a>--}}
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th colspan="4">
                <a class="ui teal labeled icon mini button" href="{{ route('admin.roles.create') }}">
                    <i class="plus icon"></i>
                    Add Role
                </a>
            </th>
        </tr>
        </tfoot>
    </table>
</div>
@stop