<?php

/**
 * Model WorkRecord
 *
 */
class WorkRecord extends Model
{


    /**
     * @var array
     */
    public static $create_rules = array(
        'user_id' => 'required',
        // project_id and task_id are left out so that the user can "Quick Start" a WorkRecord
    );


    /**
     * @var array
     */
    protected $guarded = array('id');


    /**
     * @var WorkRecordPresenter
     */
    protected $presenter;


    /**
     * To get formatted output of fields
     *
     * @return WorkRecordPresenter
     */
    public function present()
    {
        return (is_null($this->presenter) ? new WorkRecordPresenter($this) : $this->presenter);
    }


    /**
     * Get Project
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo('Project');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function task()
    {
        return $this->belongsTo('Task');
    }


    /**
     * Get work sessions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workSessions()
    {
        return $this->hasMany('WorkSession');
    }


    /**
     * @return bool
     */
    public function isRunning()
    {
        // Note: If there are no WorkSessions created, startTracking() was not yet called, so we return false
        return (is_null($this->end) && $this->workSessions()->get()->count());
    }


    /**
     * Get Customers of provided company
     *
     * @param $query
     * @param User|int $user
     * @param string $date Date format YYYY-mm-dd
     * @return mixed
     */
    public function scopeOfUser($query, $user, $date = '')
    {
        $user_id = ($user instanceof User) ? $user->id : $user;
        $query = $query->whereUserId($user_id);
        if ($date) {
            $from = date('Y-m-d 00:00:00', strtotime($date));
            $to = date('Y-m-d 23:59:59', strtotime($date));
            $query->where('start', '>=', $from)->where('start', '<=', $to);
        }

        return $query->get();
    }


    /**
     * Start a new WorkSession for this record
     *
     */
    public function startTracking()
    {
        // Do nothing if we are already running
        if ($this->isRunning()) return;

        // Any other workRecords currently running need to be stopped
        $this->stopOtherWorkRecords();

        $work_session = new WorkSession(array('start' => date('Y-m-d H:i:s')));
        $this->workSessions()->save($work_session);

        // If this was the first work session, we also add the start time to our start field
        if (is_null($this->start)) $this->start = $work_session->start;
        $this->end = null;
        $this->save();
    }


    /**
     * Stop tracking time on this WorkRecord, e.g. finish the latest WorkSession
     *
     */
    public function stopTracking()
    {
        // Do nothing if we aren't running
        if ( ! $this->isRunning()) return;

        $date = date('Y-m-d H:i:s');

        /** @var WorkSession $latest */
        $latest = $this->workSessions()->whereNull('end')->first();
        if ($latest) {
            $latest->end = $date;
            $latest->save();

            // Calculate the total time in seconds based on the timestamp of the stopped working session
            $seconds = strtotime($latest->end) - strtotime($latest->start);
            $this->time += $seconds;
        }

        // Update also cache end field
        $this->end = $date;
        $this->save();
    }


    /**
     * Also delete WorkSession objects
     */
    public function delete()
    {
        parent::delete();
        /** @var WorkSession $work_session */
        foreach ($this->workSessions as $work_session) {
            $work_session->delete();
        }
    }

    /**
     * This setter makes sure when getting the time, the value is accurate.
     * If the WorkRecord is running, it recalculates the time based on the running worksession
     *
     */
    public function getAccurateTime()
    {
        if ( ! $this->isRunning()) {
            return $this->time;
        }

        // There is a WorkSession tracking time atm
        $work_session = $this->workSessions()->whereNull('end')->first();
        $seconds = time() - strtotime($work_session->start);
        return $this->time + $seconds;
    }


    /**
     * Stops all other running working records of the user where this record belongs to
     *
     */
    protected function stopOtherWorkRecords()
    {
        $others = WorkRecord::where('user_id', '=', $this->user_id)
            ->where('id', '!=', $this->id)
            ->whereNull('end')->get();

        /** @var WorkRecord $work_record */
        foreach ($others as $work_record) {
            $work_record->stopTracking();
        }
    }

}