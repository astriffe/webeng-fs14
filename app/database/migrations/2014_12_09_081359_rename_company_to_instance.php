<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCompanyToInstance extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('companies', 'instances');

        Schema::table('customers', function (Blueprint $table) {
            $table->renameColumn('company_id', 'instance_id');
        });

        Schema::table('tasks', function (Blueprint $table) {
            $table->renameColumn('company_id', 'instance_id');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('company_id', 'instance_id');
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('instances', 'companies');

        Schema::table('customers', function (Blueprint $table) {
            $table->renameColumn('instance_id', 'company_id');
        });

        Schema::table('tasks', function (Blueprint $table) {
            $table->renameColumn('instance_id', 'company_id');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('instance_id', 'company_id');
        });
    }

}
