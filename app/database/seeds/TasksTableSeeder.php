<?php

class TasksTableSeeder extends Seeder {

	public function run()
	{

        DB::table('tasks')->truncate();

        foreach(range(1, 10) as $i)
		{
			Task::create(array(
                'company_id' => $i,
                'title' => "Task $i",
                'cost' => $i * 10,
            ));
		}
	}

}