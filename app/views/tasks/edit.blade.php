@extends('layouts.default')
@section('content')
<div class="sixteen wide column">

    <h1 class="ui header">Edit Task</h1>

    <div class="ui form segment">
        {{ Form::model($task, array('method' => 'PUT', 'route' => array('tasks.update', $task->id))) }}
        @include('tasks._form')
        <button class="ui teal submit primary button">Update</button>
        {{ Form::close() }}
    </div>

</div>
@stop