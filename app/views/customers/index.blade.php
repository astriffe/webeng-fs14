@extends('layouts.default')
@section('content')
<div class="sixteen wide column">
<h1 class="ui header">Clients</h1>
    <table class="ui sortable table segment">
        <thead>
            <tr>
                <th class="ascending">Company</th>
                <th>Street</th>
                <th>ZIP</th>
                <th>City</th>
                <th>Phone</th>
                <th>E-Mail</th>
                <th class="actions">Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($customers as $customer)
            <tr>
                <td>{{ $customer->title }}</td>
                <td>{{ $customer->street }}</td>
                <td>{{ $customer->zip }}</td>
                <td>{{ $customer->city }}</td>
                <td>{{ $customer->tel }}</td>
                <td>{{ $customer->email }}</td>
                <td class="actions">
                    <a href="{{ route('customers.edit', array($customer->id)) }}" class="ui mini icon button"><i class="icon edit"></i></a>
                    {{--<a href="#" class="ui mini icon button red"><i class="icon trash"></i></a>--}}
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th colspan="7">
                <a class="ui teal labeled icon mini button" href="{{ route('customers.create') }}">
                    <i class="plus icon"></i>
                    Add Client
                </a>
            </th>
        </tr>
        </tfoot>
    </table>
</div>
@stop