\newcommand{\lecture}{Web Engineering\\
HS 2014} 
\newcommand{\serieNumber}{\\} 

\newcommand{
\authorsOfSerie}{Alexander Striffeler, 07-918-691\\}

\author{Alexander Striffeler (a.striffeler@students.unibe.ch)\\Stefan Wanzenried (stefan.wanzenried@gmail.com) \\ \hfill \\ Web Engineering, Universities BeNeFri}



\documentclass[a4paper]{article} 
\usepackage{../documentstyle}
\usepackage{float}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{xcolor}

\headheight=40pt

\lstset{
	language=bash,
	breaklines=true,
	emptylines=1,
	basicstyle=\ttfamily\color{black},
	frame=none,
	numbers=none,
	showspaces=false,
	showstringspaces=false,
}

%=================================
%	Warning Box definition
%=================================
\usepackage{framed}
\definecolor{peach}{rgb}{1.0, 0.9, 0.71}
\definecolor{platinum}{rgb}{0.9, 0.89, 0.89}
\renewenvironment{leftbar}[1][\hsize]%
{%
        \def\FrameCommand%
        {%
             \includegraphics[width=1.2cm]{fig/pitfall.png}%           
             \fboxsep=\FrameSep\colorbox{platinum}%
        }%
        \MakeFramed{\hsize#1\advance\hsize-\width\FrameRestore}%
}%
{\endMakeFramed}


%=====================================
%	Begin Document
%=====================================
\begin{document} 
\input{../header} 

\title{Chronos Time Tracking\\Project Report}

\maketitle
\tableofcontents


\section{Introduction}
To work efficiently and to keep control of your business' expenses, it is crucial to monitor how much time you or your employees spent for a specific project or even task. \textit{Chronos} helps you tracking time for all of your customers, projects and tasks in a fine-grained way. Meanwhile, our main focus was on delivering an application that allows quick an easy tracking once it is set up, since: We don't want you to track the time you need to track your time!


\section{Development Platform}\label{se:platform}
This section will give you a short overview on which components we have used for building and running \textit{Chronos} during development. Figure \ref{fig:components} shows how these components interact.

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.3]{./fig/components.png}
\caption{System Component Overview}
\label{fig:components}
\end{figure}

\subsection{Programming Language and Framework}
We have decided to build \textit{Chronos} using the PHP-Based framework Laravel\footnote{http://www.laravel.com}. Laravel is a modern web application framework with elegant and expressive syntax. It also provides the developer with a bunch of helpful features out of the box to efficiently develop a modern web application, such as:
\begin{itemize}
\item RESTful routing
\item ORM and migration system to connect to a variety of databases without hassle.
\item Templating: The blade templating engine allows pages to inherit code from others. This effectively avoids code duplication in the frontend. 
\item Composer: Dependency manager in PHP. Laravel uses Composer to manage third-party packages.
\end{itemize}


\subsection{Operating System}
The technologies we have used do not restrict your choice of operating system. While we have used Mac OS and Ubuntu during development, you are also free to use Windows or other Linux flavours on your machine since all components are available for these operating systems aswell. However, please consider that the installation process might slightly differ from what we describe in section \ref{se:installation}. Please note that in this document, for the sake of simplicity, we will subsequently assume you are working on a Linux flavour such as Ubuntu and name the relevant bash commands for this operating system. 

\subsection{Web Server}
There are only few requirements the web server needs to match in order to serve \textit{Chronos}, as they are:
\begin{itemize}
\item PHP $\geq 5.4$
\item MCrypt PHP Extension
\item (As of PHP 5.5, some Operating Systems may require you to manually install the PHP JSON extension. Using Ubuntu, you can achieve this by installing the package \verb+php5-json+.
\end{itemize}

Typically, one may use one of the commonly used web servers such as Apache\footnote{httpd://apache.org} or Nginx\footnote{http://nginx.org} for productive instances. For development, however, we have used PHP's built-in development server, which, once the system is set up, can easily be started using the command:

\begin{lstlisting}
  $ php artisan serve
\end{lstlisting}

As our application will typically run on a web server, we do not refuse http request at application level. We think that it is smarter to restrict or redirect such access at the webserver itself. This allows the administrator for instance to restrict access to https from the internet but to allow http from the local network.

\subsection{Database}
We have decided to use MariaDB\footnote{https://mariadb.org} as persistency layer of our application. As MariaDB is a fork of MySQL and can be used as a binary drop-in replacement for the latter in the same version, those two technologies can easily be exchanged without any hassle. Further details about Feature sets and some few incompatibilities can be found on their website.

During the development phase, we chose to use a local MariaDB instance. However, when going productive, the use of a dedicated database server is just one config-file away.


\subsection{Installation}\label{se:installation}
As mentioned above, we will limit the installation procedure to Linux systems such as Ubuntu. Other operating systems will provide similar procedures, detailed installation descriptions can be found on the websites of the specific component. We will further assume that you are using Ubuntu 14.04 or newer since the packet repositories are different for older versions.

First, you will install the programming language, PHP 5 in our case. To do this, type 
\begin{lstlisting}
  $ sudo apt-get install php5 -y
\end{lstlisting}
For later being able to connect to the database, we also need to install the PHP5 MySQL connector, which is included in the base installation on MacOS but unfortunately not in the Ubuntu package.
\begin{lstlisting}
  $ sudo apt-get install php5-mysql -y
\end{lstlisting}

Having PHP installed, we will now move on to the database installation:
\begin{lstlisting}
  $ sudo apt-get install mariadb-server -y
\end{lstlisting}

Later, we will add user and database for our application. 
Concerning platform installation, the next step to take is to download Laravel, our PHP framework. As this procedure relies on Composer\footnote{https://getcomposer.org}, we first need to execute
\begin{lstlisting}
  $ curl -sS https://getcomposer.org/installer | php
\end{lstlisting}

Once this is done, we can download the Laravel installer using Composer:
\begin{lstlisting}
  $ composer global require "laravel/installer=~1.1"
\end{lstlisting}

Make sure to place the \~/.composer/vendor/bin directory in your PATH so the laravel executable is found when you run the laravel command in your terminal.

Once installed, you are free to create a new Laravel application if you desire. The simple laravel new command will create a fresh Laravel installation in the directory you specify. For instance, 
\begin{lstlisting}
  $ laravel new webApplication
\end{lstlisting}
laravel new webApplication would create a directory named \textit{webApplication} containing a fresh Laravel installation with all dependencies installed. This method of installation is much faster than installing via Composer. As this guide will show you how to start our existing \textit{Chronos} project however, concerning Platform installation, you are all set by now. Congratulations!


\section{Development Environment}
\subsection{IDE}
For developing PHP, there are hardly any limitations on how to write your code. Starting at most basic editors such as vi, vim, gedit or jedit, one can also use more sophisticated development environments such as PHPStorm\footnote{https://www.jetbrains.com/phpstorm}. The latter gives you a far more convenient view on your project as a whole and some ease-of-use such as code-completion, which is not the case for text editors. However, you are free to use the program you are most familiar with.

\subsection{Version Control}
During the development of \textit{Chronos}, we have been using Git\footnote{http://www.git-scm.com} to version control our application data. The repository, which was hosted on Bitbucket\footnote{http://www.bitbucket.org} allowed us to 
\begin{itemize}
\item conveniently maintain different states of the project,
\item develop several features independently and
\item track, assign and organize tasks and issues.

\end{itemize}

Besides this, Git allows for reverting specific changes or detecting where erroneous code was added.
If you wish, you can check out our repository by typing
\begin{lstlisting}
  $ git clone https://astriffe@bitbucket.org/astriffe/webeng-fs14.git directoryOfYourChoice/
\end{lstlisting}


\section{Presentation Layer}

\subsection{Type of Interface}
The app runs in the latest modern browsers (Chrome, Firefox, Safari etc.) and should also run on Internet Explorer $\geq 10$ although we did not test it.
\textit{Chronos} uses a \emph{Thin client} architecture, as all the business logic is handled server side. The only small logic that is handled client-side with javascript happens when handling the response of AJAX requests which are used to start and stop tracking tasks.

\subsection{Technology Choice}
Tracking time of various tasks is handled with AJAX, allowing the user to quickly switch between active tasks without reloading the page. The rest of the application (managing users, projects, clients and tasks) uses normal HTTP requests.


\subsection{Interface Realization}
With Blade, Laravel provides a simple, yet very powerful templating engine. Blade is driven by template inheritance and sections. Thus, Blade templates can be included and/or extended on each page. Together with powerful functional annotations such as conditionals or loops, these templates show up to be a convenient way to render complex pages while avoiding code duplication if applied well.

\subsection{Frontend}
The frontend is powered by Semantic UI\footnote{\url{http://semantic-ui.com/}}, a very new and sexy CSS framework. In fact, version 1.0 was released right before the deadline of this project (\textit{Chronos} still uses a beta version). Figure \ref{fig:screenshot} shows how the frontend currently looks. In contrast to other frameworks like Bootstrap or Foundation, Semantic UI is structured around natural language conventions to make development more intuitive. As an example, to generate a small vertical menu, one can use the following CSS classnames
\begin{lstlisting}
<div class="ui small vertical menu">
\end{lstlisting}

\begin{figure}[htbp]
\centering
\includegraphics[scale=0.45]{./fig/chronos_tracking_time.png}
\caption{Frontend screenshot}
\label{fig:screenshot}
\end{figure}


\section{Logic Layer}
According to the commonly used MVC (Model-View-Controller) pattern, all the business logic of our application is placed in the Logic layer. 

\subsection{Object Model}
We have created an object-oriented model of our business domain. With this model, we can easily delegate functionality to the appropriate object and thus achieve good encapsulation. Figure \ref{fig:uml} shows how the classes are interconnected in a simplified UML diagram.

\begin{figure}[htbp]
\begin{centering}
\includegraphics[scale=0.28]{fig/uml.png}
\caption{Simplified UML-Diagram of our domain model.}
\label{fig:uml}
\end{centering}
\end{figure}


\subsection{Database}
During development, we have used a local database server in order to reduce dependencies on external systems and network connections. This way, we were able to independently modify the database according to the needs of the current task. 

Of course, the users passwords are stored in hashed form for security reasons. Thus, changing the application key as described in section \ref{se:installationguide} invalidates all password hashes stored in the database. Hence, this command sould be used with a maximum of precaution.

\subsubsection{ORM}
The application makes use of Laravel's built in ORM called \emph{Eloquent}, a simple ActiveRecord implementation for working with the database. Each database table has a corresponding model which is used to interact with the table.
Eloquent works similar to the ActiveRecord built into Ruby on Rails with features like query scopes, relationships, eager loading and collections. As an example, we take a look at our role model. A role is assigned to many users and grants some permissions. These relations are represented in Laravel with the following methods:
\begin{lstlisting}
    /**
     * Return all Users assigned to this role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('User');
    }
\end{lstlisting}

\begin{lstlisting}
    /**
     * Return all Permissions of this role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany('Permission');
    }
\end{lstlisting}


\subsection{Multiple instances}

\textit{Chronos} is able to support multiple instances with one installation and database. One instance holds users, customers and tasks. If a user logs into the system, the application only shows data associated with the user's instance. This allows us to host multiple clients using \textit{Chronos} with only one installation.

\subsection{Roles and Permissions}
\emph{Chronos} implements a role based access control system. Each user can have multiple roles, each role giving certain permissions to perform an action. Table \ref{tbl:roles_permissions} shows which roles have which permissions and gives a short description on what kind of users should get which role. Permissions are checked in the controllers. If a user tries to perform an action without having the privilege to do so, the application aborts with a 403 HTTP status code and displays an error message.

\subsubsection{Roles}

\begin{table}[htbp]
\begin{tabular}{@{}p{3cm}p{8cm}p{4cm}@{}}
\toprule
\textbf{Role}  & \textbf{Description}                                                                               & \textbf{Permissions}                                                                         \\ \midrule
superadmin     & This role grants access to everything                                                              & administrate\_instances \newline administrate\_users \newline administrate\_roles \newline administrate\_permissions \\ \midrule
admin          & Manage instances and users without the permissions to edit roles and permissions                   & administrate\_instances \newline administrate\_users                                                 \\ \midrule
instanceowner   & Full rights on instance level. Manage users, clients, tasks and projects belonging to one instance & manage\_users \newline manage\_projects \newline manage\_customers \newline manage\_tasks \newline report\_user \newline track\_time \\ \midrule
projectmanager & Manage only projects and tasks of a given instance                                                 & manage\_projects \newline manage\_tasks \newline report\_user \newline track\_time                                   \\ \midrule
user           & Basic role, just gives access to track the time and view own reports                               & track\_time \newline report\_user                                                                    \\ \bottomrule
\end{tabular}
\caption{The different roles and the permissions that are assigned to.}
\label{tbl:roles_permissions}
\end{table}


Note that the roles \emph{superadmin} and \emph{admin} are system roles and therefore only assignable to users by other administrators. A user with the permission \emph{manage\_users}, currently obtained by the role \emph{instanceowner}, is able to assign roles to users on instance level.


\subsubsection{Permissions}

The following permissions are defined:

\begin{itemize}
\item administrate\_instances
\item administrate\_users
\item administrate\_roles
\item administrate\_permissions
\item manage\_users
\item manage\_projects
\item manage\_customers
\item manage\_tasks
\item report\_user
\item track\_time
\end{itemize}

If a new permission is introduced, it first needs to be integrated into the application code, e.g. checking if the currently logged in user has the new permission to perform an action. Secondly, the permission needs to be attached to at least one role.

%\section{Code Documentation Tools}


\section{Installation Guide}\label{se:installationguide}
As we talk about a web application and our environment has already been set up in section \ref{se:platform}, the installation of the application will not take long.

Most probably, you already have a web browser installed. This is the only client-side-requirement to access \textit{Chronos}. If you do not and if you are using Ubuntu, for instance to install firefox, you can simply type
\begin{lstlisting}
  $ sudo apt-get install firefox
\end{lstlisting}
You are free to use any other web browser of your choice.

If you want to run \textit{Chronos} locally or if you are working on a server, you will next download our application. When working on the server, you will probably want to download the repository into your web root or the appropriate subdirectory directly.
\begin{lstlisting}
  $ git clone https://astriffe@bitbucket.org/astriffe/webeng-fs14.git <directoryOfYourChoice>/
  $ cd <directoryOfYourChoice>/
\end{lstlisting}

Next, you will have to set up the database. You will login as user \verb+root+ with the password you have set during the installation process of MariaDB. With the following commands, we will install the MySQL client on our machine, create a database and grant the appropriate rights to the new user. You can also use different SQL clients to perform these tasks. For development, the database user was called \verb+webeng+ with password \verb+webeng+, which is also the applications default configuration.
\begin{lstlisting}
  $ sudo apt-get install mysql-client -y
  $ mysql -u root -p <your password>
  mysql> CREATE DATABASE webeng;
  mysql> CREATE USER `webeng'@`localhost' IDENTIFIED BY `password';
  mysql> GRANT ALL PRIVILEGES ON webeng.* TO `webeng'@`localhost';
  mysql> FLUSH PRIVILEGES;
\end{lstlisting}

\textit{NB: } Of course, you can (and should!) change username and password for the database user. These settings then need to be adopted in the file \verb+$PROJECT_HOME/app/config/database.php+. If you wish to use a different database server, you change these settings there aswell.

You are now ready to load the database structure of \textit{Chronos}. If you application runs in production mode, you will get warnings when executing the following commands. Please confirm those. To load the DB structure, navigate to the project home folder and execute
\begin{lstlisting}
  $ php artisan migrate
\end{lstlisting}

For security reasons, please make sure to to protect your application key. If you have to generate a new key, you can do this by invoking the command 
\begin{lstlisting}
  $ php artisan key:generate
\end{lstlisting}
\begin{leftbar}[0.9\linewidth]
Be aware that passwords that were generated prior to this step using \verb+Hash::make()+ will get invalidated if a new key is generated.
\end{leftbar}


Next, you have to populate the database with some initial data, you can use the following command. 
\begin{lstlisting}
  $ php artisan db:seed
\end{lstlisting}
Besides the defined user roles and privileges, in this version of the application, the \verb+db:seed+ command generates two users, namely superadmin and admin with the following credentials:
\begin{lstlisting}[]
        admin:    admin
   superadmin:	  superadmin
\end{lstlisting}
\begin{leftbar}[0.9\linewidth]
Every time you run \verb+php artisan db:seed+, the database is being truncated. Thus, you should not run this command on an existing installation.
\end{leftbar}

If you log in with those users, you can then create new instances, add users, projects and tasks to them and start tracking.

Now you are all set and ready to run the application on the built-in server for the first time by simply typing
\begin{lstlisting}
  $ php artisan serve
  Laravel development server started on http://localhost:8000
\end{lstlisting}
Open your browser, go to localhost:8000 and enjoy!


\section{User Guide}

\subsection{Administration Tasks}
\subsubsection{User Accounts}
As \textit{Chronos} is designed to serve several instances, the user can not create a new account himself. Instead, a \verb+superadmin+, \verb+admin+ or an existing \verb+instanceowner+ has to provide an account for each user. These instances are also able to assign roles to each user account which then permit the specific user to perform a specific set of actions.

\subsubsection{Client and Project Management}
Once an instance owner receives his credentials from a system administrator, he is able to add customer information to the system in order to attach projects to a specific client. Please note that every project must be assigned to a client (while, of course, a company might maintain dummy client objects). Of course, the scope of these object is limited to the specific instance which avoids users to see client or project information from other instances.

\subsubsection{Tasks}
The \verb+instanceowner+ or one of his \verb+project_managers+ can now define a set of tasks. When tracking time, a \verb+user+ can later chose a project and a task he is working on. By generating a well thought task set, the management can govern the report to detect which phases and / or parts of the project took how long.

\subsection{Tracking Time}
Once the account for a general user is set up by the \verb+instanceowner+ and the project data has been added, each user of this instance can start tracking it's time spent on different tasks. The procedure is easy: After signing on with his username and password, the user can chose the project and the task he starts working on. In addition, he can add more detailed description on the current work in the field ` `Remarks'. By clicking the button `Track', a new time record is being created at the bottom of the page. To stop this record, a user has two options:
\begin{enumerate}
\item Create a new entry: By again choosing a project, task and possibly remarks and hitting `Track', the old record is stopped and a new active record is being created. There can always be only one active record at a time for every user. The active record is highlighted in green color.
\item Click the green button that displays the elapsed time in the section `Tracked Time'. Clicking these time buttons toggles the specific entry active or passive. Thus, after a break, the user can simply continue the last task by clicking on the button showing the elapsed time.
\end{enumerate}

If things have to go fast, a user can simply hit the 'Track' button. \textit{Chronos} will now create a Quick-Start entry. Information on client, task and remarks can be added later.

At the bottom of the `Track Time' page, the section `Tracked Time' shows all the time records of a specific day. The day to be displayed can be set either by choosing one of the last five days suggested at the top of the page or by choosing a specific day using the date picker.

\subsection{Profile}
In the profile section indicated by the current users username, the user can either end the current session by logging out or editing his profile information such as username and password. The instance a user belongs to can only be changed by administrators. 


\newpage
\section{Conclusion}
We believe that \textit{Chronos} delivers an easy and attractive way to monitor the time one spent on specific tasks and / or projects at this stage. However, as for most projects, there are many opportunities to extend or enhance \textit{Chronos}, some of which could be
\begin{itemize}
\item \textbf{Dynamic Reporting: }We can think of having a fine-grained reporting system that allows a user to clearly select what he wants to examine in what form. For instance, we can think of both having a weekly report for the user himself to see how much he has worked. On the other hand, a user might want to be able to select a project and print only certain tasks. Dynamic reporting would allow a user to precicely extract the data he desires.
\item \textbf{Export: } Export the created reports as XLS, CSV or PDF in order to use them in other application or simply for documentation purposes.
\item \textbf{Import: }There are several platforms to track time on mobile devices. By handling CSV data, such existing platforms could be used to integrate the time data of your field crew aswell.
\item \textbf{Cost Control: } Once both dynamic reporting and export functionality are achieved, having cost and billability information for each task, a user could export the appropriate report as PDF and directly use it as invoice for his clients. The bill is ready in a minute - monthly, weekly or even daily.

\end{itemize}


\end{document}
