@extends('layouts.error')
@section('content')
    <div class="sixteen wide column">
    <div class="ui error message">
        <i class="close icon"></i>
        <div class="header">
            Permission denied
        </div>
        Sorry, but you don't have access to this page. Maybe you'd like to move a step <a href="{{ URL::previous() }}">back</a>?
    </div>
    </div>
@stop