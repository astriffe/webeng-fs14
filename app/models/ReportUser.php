<?php

/**
 * Class ReportUser
 * Does provide some methods to deliver report data of a user
 *
 */
class ReportUser
{

    /**
     * @var User
     */
    protected $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    /**
     * Get some data of a project
     *
     * @param int $project_id
     * @return array
     */
    public function getDataByProject($project_id)
    {
        // Get all the WorkRecords of the given user and project
        $records = WorkRecord::where('user_id', '=', $this->user->id)
            ->where('project_id', '=', $project_id)
            ->orderBy('task_id')->get();

        // Sum up total time
        $total = 0;
        /** @var WorkRecord $record */
        foreach ($records as $record) {
            $total += $record->getAccurateTime();
        }

        return (object) array('records' => $records, 'total' => WorkRecordPresenter::format($total));
    }


}