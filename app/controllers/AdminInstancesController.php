<?php

class AdminInstancesController extends \BaseController
{


    public function __construct()
    {
        parent::__construct();
        
        $this->beforeFilter(function () {
            if ( ! $this->user->hasPermission('administrate_instances')) {
                App::abort(403);
            }
        });
    }


    /**
     * Display a listing of users
     *
     * @return Response
     */
    public function index()
    {
        $instances = Instance::all();

        return View::make('admin_instances.index', compact('instances'));
    }


    /**
     * Show the form for creating a new user
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin_instances.create');
    }


    /**
     * Store a newly created user in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            Instance::create(Input::all());
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('admin.instances.index')->with('success', 'Successfully created Instance ' . Input::get('title'));

    }


    /**
     * Display the specified instance.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $user = Instance::findOrFail($id);

        return View::make('admin_instances.show', compact('instance'));
    }


    /**
     * Show the form for editing the specified user.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $instance = Instance::findOrFail($id);

        return View::make('admin_instances.edit', compact('instance'));
    }


    /**
     * Update the specified user in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $instance = Instance::findOrFail($id);
        try {
            $instance->update(Input::all());
        } catch (ValidationException $e) {
            return Redirect::back()->withInput(Input::all())->withErrors($e->getErrors());
        }

        return Redirect::route('admin.instances.index')->with('success', 'Successfully updated Instance ' . Input::get('title'));

    }


    /**
     * Remove the specified user from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        Instance::destroy($id);

        return Redirect::route('admin_instances.index');
    }

}
