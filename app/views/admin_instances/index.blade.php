@extends('layouts.default')
@section('content')
<div class="sixteen wide column">
<h1 class="ui header">Instances</h1>
    <table class="ui sortable table segment">
        <thead>
            <tr>
                <th class="ascending">Company</th>
                <th>Street</th>
                <th>ZIP</th>
                <th>City</th>
                <th>Phone</th>
                <th>E-Mail</th>
                <th class="actions">Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach($instances as $instance)
            <tr>
                <td>{{ $instance->title }}</td>
                <td>{{ $instance->street }}</td>
                <td>{{ $instance->zip }}</td>
                <td>{{ $instance->city }}</td>
                <td>{{ $instance->tel }}</td>
                <td>{{ $instance->email }}</td>
                <td class="actions">
                    <a href="{{ route('admin.instances.edit', array($instance->id)) }}" class="ui mini icon button"><i class="icon edit"></i></a>
                    {{--<a href="#" class="ui mini icon button red"><i class="icon trash"></i></a>--}}
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <th colspan="7">
                <a class="ui teal labeled icon mini button" href="{{ route('admin.instances.create') }}">
                    <i class="plus icon"></i>
                    Add Instance
                </a>
            </th>
        </tr>
        </tfoot>
    </table>
</div>
@stop