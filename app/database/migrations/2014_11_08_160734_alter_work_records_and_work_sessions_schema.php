<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWorkRecordsAndWorkSessionsSchema extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE work_sessions MODIFY end TIMESTAMP NULL');

        // Create cache field holding the total time for a work record with start/end of all workSessions
        Schema::table('work_records', function($table) {
            $table->integer('time')->unsigned();
            $table->dateTime('start')->nullable();
            $table->dateTime('end')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('ALTER TABLE work_sessions MODIFY end TIMESTAMP NOT NULL');
	}

}
