<?php

class Role extends Model {

    /**
     * System-roles
     */
    const ROLE_SUPERADMIN = 1;
    const ROLE_ADMIN = 2;

    /**
     * @var array
     */
    public static $rules = array(
        'title' => 'required',
        'name' => 'required|regex:/^[a-z_]+$/'
    );


    /**
     * @var array
     */
    protected $guarded = array('id');


    public function scopeInstance($query)
    {
        return $query->whereSystem(0);
    }


    public function scopeSystem($query)
    {
        return $query->whereSystem(1);
    }

    /**
     * Return all Users having this role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('User');
    }


    /**
     * Return all Permissions of this role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany('Permission');
    }

}