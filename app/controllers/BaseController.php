<?php

class BaseController extends Controller
{

    /**
     * @var User
     */
    protected $user;


    public function __construct()
    {
        // TODO Maybe create a guest user object
        $user = (Auth::user()) ? Auth::user() : new User();
        $this->user = $user;

        // Share the user object in views
        View::share('current_user', $user);

        // Pass some configuration to the view and make it available for javascript
        $this->shareConfigInView();
    }


    /**
     * Pass JSON encoded config to view
     */
    protected function shareConfigInView()
    {
        $config = array();
        $config['urls']['tracking'] = str_replace('{date?}', '', urldecode(route('track.index')));
        $config['urls']['start_tracking'] = str_replace('{id}', '', urldecode(route('track.start')));
        $config['urls']['stop_tracking'] = str_replace('{id}', '', urldecode(route('track.stop')));
        $config['urls']['get_accurate_time'] = str_replace('{id}', '', urldecode(route('track.accurate_time')));
        $config['urls']['update_work_record'] = str_replace('{id}', '', urldecode(route('track.update')));
        $config['urls']['delete_work_record'] = str_replace('{id}', '', urldecode(route('track.delete')));
        View::share('js_config', json_encode($config));
    }


    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if ( ! is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }


//    public function update_variables()
//    {
//        // Quote: Sharing is caring
//        if (Auth::user()) {
//            $role = Auth::user()->role();
//            View::share('role', $role);
//
//            View::share('isUser', True);
//            if ($role > 1) {
//                View::share('isProjectManager', True);
//                if ($role > 2) {
//                    View::share('isCompanyOwner', True);
//
//                    if ($role > 3) {
//                        View::share('isAdmin', True);
//                        if ($role > 4) {
//                            View::share('isSuperAdmin', True);
//                        } else { // Not Superadmin
//                            View::share('isSuperAdmin', False);
//                        }
//                    } else { // Not Admin
//                        View::share('isAdmin', False);
//                    }
//                } else { // Not Company Owner
//                    View::share('isCompanyOwner', False);
//                }
//            } else { // Not Project Manager
//                View::share('isProjectManager', False);
//            }
//        } else {
//            View::share('role', 0);
//            View::share('isUser', False);
//            View::share('isProjectManager', False);
//            View::share('isCompanyOwner', False);
//            View::share('isAdmin', False);
//            View::share('isSuperAdmin', False);
//        }
//    }

}
